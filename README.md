# SimpleWMS

eine einfache Lagerverwaltung

verwendete Technologien:

+ Java 8
+ Spring Boot 2
+ Spring MVC
+ Thymeleaf
+ Spring Data JPA
+ H2 in memory DB
+ MySQL/MariaDB
+ Bootstrap 4

verwendete Tools:

+ Spring Tool Suite 4 (Eclipse)
+ Maven
+ Project Lombok
+ Git
+ Spring Boot DevTools
 
in der aktuellen Version aufgeteilt in mehrere Teilprojekte:

+ [simple-wms-backend](https://gitlab.com/dstieghorst/simple-wms-backend) - Backend mit Datenbankzugriff
+ SimpleWMS - nur noch als Web-Anwendung, kommuniziert mit dem Backend über REST, mit Spring Security gesichert
+ [simple-wms-rest-api](https://gitlab.com/dstieghorst/simple-wms-rest-api) - REST API für mobile bzw. JavaScript Anwendungen, abgesichert mit JWT
+ [simple-wms-react](https://gitlab.com/dstieghorst/simple-wms-react) - JS-Anwendung mit React.js, zur Zeit als Proof of Concept, da noch unvollständig
 

Zum Ausprobieren sind die folgenden Schritte nötig:

+ MySQL bzw. MariaDB Datenbank-Schema "simplewms", erreichbar über
  + JDBC-Url: jdbc:mysql://localhost:3306/simplewms
  + Username: wmsuser
  + Passwort: wmspass  
 (alternativ in simple-wms-backend/src/main/resources/application.properties anpassen)
+ zunächst das Backend starten: simple-wms-backend
+ das das Web-Frontend, diese Anwendung: SimpleWMS
+ zwei User hinterlegt: "admin" und "tester"
+ Default-Passwort "password"


[Mehr im Wiki ...](https://gitlab.com/dstieghorst/SimpleWMS/wikis/home) (Information dort zum Teil überholt)