package info.stieghorst.simpleWMS.domain;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Predicate;

public class User {
		
		// fields

		private Long id;
		
		private String username;
		
		private String password;
		
		private boolean accountExpired;
		
		private boolean accountLocked;
		
		private boolean credentialsExpired;
		
		private boolean enabled;
		
		private Set<PickingOrder> orders;
		
		
		private Set<Role> roles = new HashSet<>();
		
		public User() {
			
		}

		public User(String username, String password) {
			this.username = username;
			this.password = password;
		}
		
		public void addRole(Role role) {
			Predicate<Role> predicate = r -> ((r.getId() == role.getId()) || (r.getRoleName() == role.getRoleName()));
			this.roles.removeIf(predicate);
			this.roles.add(role);
			role.getUsers().add(this);
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public boolean isAccountExpired() {
			return accountExpired;
		}

		public void setAccountExpired(boolean accountExpired) {
			this.accountExpired = accountExpired;
		}

		public boolean isAccountLocked() {
			return accountLocked;
		}

		public void setAccountLocked(boolean accountLocked) {
			this.accountLocked = accountLocked;
		}

		public boolean isCredentialsExpired() {
			return credentialsExpired;
		}

		public void setCredentialsExpired(boolean credentialsExpired) {
			this.credentialsExpired = credentialsExpired;
		}

		public boolean isEnabled() {
			return enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}

		public Set<Role> getRoles() {
			return roles;
		}

		public void setRoles(Set<Role> roles) {
			this.roles = roles;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((id == null) ? 0 : id.hashCode());
			result = prime * result + ((password == null) ? 0 : password.hashCode());
			result = prime * result + ((username == null) ? 0 : username.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			User other = (User) obj;
			if (id == null) {
				if (other.id != null)
					return false;
			} else if (!id.equals(other.id))
				return false;
			if (password == null) {
				if (other.password != null)
					return false;
			} else if (!password.equals(other.password))
				return false;
			if (username == null) {
				if (other.username != null)
					return false;
			} else if (!username.equals(other.username))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "User [id=" + id + ", username=" + username + ", password=" + password + "]";
		}
		
}
