package info.stieghorst.simpleWMS.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import info.stieghorst.simpleWMS.domain.User;
import info.stieghorst.simpleWMS.service.UserService;

/*
 * Spring Security with login form
 * copied from example code found in several sources
 */
@Service
public class WMSUserDetailsService implements UserDetailsService {
	
	private final UserService userService;

	public WMSUserDetailsService(UserService userService) {
		super();
		this.userService = userService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.getUserByName(username);
		
		if(user == null) {
			throw new UsernameNotFoundException("cannot find username: " + username);
		}
		
		return new WMSUserPrincipal(user);
	}
	
	

}
