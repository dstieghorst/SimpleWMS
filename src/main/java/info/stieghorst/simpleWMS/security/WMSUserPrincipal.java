package info.stieghorst.simpleWMS.security;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;

/*
 * Spring Security with login form
 * copied from example code found in several sources
 */
public class WMSUserPrincipal implements UserDetails {

	private static final long serialVersionUID = 1L;

	private User user;
	
	public WMSUserPrincipal(User user) {
		super();
		this.user = user;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<Role> userRoles = user.getRoles();
		if(userRoles == null || userRoles.isEmpty()) {
			return Collections.emptySet();
		}
		
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		userRoles.forEach(role -> {
			authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
		});
		
		return authorities;
	}

	@Override
	public String getPassword() {
		return this.user.getPassword();
	}

	@Override
	public String getUsername() {
		return this.user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return !(this.user.isAccountExpired());
	}

	@Override
	public boolean isAccountNonLocked() {
		return !(this.user.isAccountLocked());
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return !(this.user.isCredentialsExpired());
	}

	@Override
	public boolean isEnabled() {
		return !(this.user.isEnabled());
	}

}
