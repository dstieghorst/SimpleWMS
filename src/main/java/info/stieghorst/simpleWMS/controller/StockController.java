package info.stieghorst.simpleWMS.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import info.stieghorst.simpleWMS.domain.Location;
import info.stieghorst.simpleWMS.domain.Product;
import info.stieghorst.simpleWMS.domain.Stock;
import info.stieghorst.simpleWMS.service.LocationService;
import info.stieghorst.simpleWMS.service.ProductService;
import info.stieghorst.simpleWMS.service.StockService;
import info.stieghorst.simpleWMS.transfer.NewStockTO;
import info.stieghorst.simpleWMS.transfer.StockCorrectionTO;
import info.stieghorst.simpleWMS.transfer.StockSearchTO;
import lombok.extern.slf4j.Slf4j;

/*
 * controller to handle all stock-related pages
 */
@Slf4j
@Controller
@RequestMapping("/stock")
public class StockController {
	
	private final StockService stockService;
	
	private final ProductService productService;
	
	private final LocationService locationService;

	public StockController(StockService stockService, ProductService productService, LocationService locationService) {
		this.stockService = stockService;
		this.productService = productService;
		this.locationService = locationService;
	}
	
	/*
	 * used in connect with search field "productNo"
	 * null is allowed, but empty field is string with length 0,
	 * therefore triggering @Size error
	 * this binder converts empty string to null
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	/*
	 * list all stocks that match
	 * the entered search criteria
	 * use validation as defined in TransferStockSearch class
	 */
	@GetMapping("/list")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showStockSearchResults(
			@PageableDefault(size = 10) Pageable pageable,
			@Valid @ModelAttribute("stockSearch") StockSearchTO stockSearch, 
			BindingResult bindingResult,
			Model model) {
		log.debug("showStockSearchResults - start");
		
		if(bindingResult.hasErrors()) {
			log.error("errors in search criteria");
			for (Object object : bindingResult.getAllErrors()) {
			    if(object instanceof FieldError) {
			        FieldError fieldError = (FieldError) object;
			        log.error("FieldError: {}/{}", fieldError.getField(), fieldError.getCode());
			    }
			    if(object instanceof ObjectError) {
			        ObjectError objectError = (ObjectError) object;
			        log.error("ObjectError: {}/{}", objectError.getObjectName(), objectError.getCode());
			    }
			}
			model.addAttribute("stockSearch", stockSearch);
			model.addAttribute("lastSearch", new StockSearchTO());
			model.addAttribute("page", stockService.getStockSearchResult("", false, null, null, null, pageable));		
			return "stock/list";
		}
		
		if(stockSearch.getProductNo() != null) {
			stockSearch.setNoLocation(false);
			stockSearch.setLocAisle(null);
			stockSearch.setLocStack(null);
			stockSearch.setLocLevel(null);
		}
		else if(stockSearch.isNoLocation()) {
			stockSearch.setLocAisle(null);
			stockSearch.setLocStack(null);
			stockSearch.setLocLevel(null);
		}
		else if(stockSearch.getLocAisle() != null && stockSearch.getLocStack() == null) {
			stockSearch.setLocLevel(null);
		}
		model.addAttribute("lastSearch", stockSearch);
		model.addAttribute("page", 
				stockService.getStockSearchResult(
						stockSearch.getProductNo(), 
						stockSearch.isNoLocation(),
						stockSearch.getLocAisle(), 
						stockSearch.getLocStack(), 
						stockSearch.getLocLevel(),
						pageable));		
		
		return "stock/list";
	}
	
	/*
	 * link in the product list to create stock for a selected product
	 * collect data and open form
	 */
	@GetMapping("/{id}/createStock")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String createStock(@PathVariable String id, Model model) {
		log.debug("createStock - id = {}", id);
		
		Product product = productService.getProductById(new Long(id));
		if(product == null) {
			log.error("product not found!");
			return "stock/list";
		}
		log.debug("found product: {}", product.getProductNo());
		NewStockTO newStock = new NewStockTO(product);

		model.addAttribute("newStock", newStock);
		return "stock/createStockForm";
	}
	
	/*
	 * save newly created stock
	 * and go back to the stock list
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveNewStock(@PageableDefault(size = 10) Pageable pageable,
			@ModelAttribute NewStockTO newStock, Model model) {
		log.debug("saveNewStock - start");
		if(newStock != null) {
			Product product = productService.getProductById(newStock.getProduct().getId());

			Stock stock = new Stock();
			stock.setUnits(newStock.getUnits());
			stock.setProduct(product);
			
			stockService.saveStock(stock);
		}
		
		model.addAttribute("stockSearch", new StockSearchTO());
		model.addAttribute("lastSearch", new StockSearchTO());
		model.addAttribute("page", stockService.getStockSearchResult("", false, null, null, null, pageable));		
		return "stock/list";
	}
	
	/*
	 * stock movement
	 * 1st step: show form to select the new location
	 */
	@GetMapping("/{id}/move")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showStockMovementForm(@PathVariable String id, Model model) {
		log.debug("showStockMovementForm - id = {}", id);
		
		Stock stock = stockService.getStockById(new Long(id));
		List<Location> locationList = locationService.getEmptyLocationList(); 
		
		model.addAttribute("stock", stock);
		model.addAttribute("locationList", locationList);
		return "stock/move";
	}
	
	/* 
	 * stock movement
	 * 2nd step: book stock to the selected location
	 */
	@GetMapping("/{id}/{id2}/selectNewLocation")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String setNewLocation(@PathVariable String id, @PathVariable String id2, Model model) {
		log.debug("setNewLocation - id = {}", id);
		log.debug("setNewLocation - id2 = {}", id2);
		
		Stock stock = stockService.getStockById(new Long(id));
		Location newLocation = locationService.getLocationById(new Long(id2));
		
		model.addAttribute("newLocation", newLocation);
		model.addAttribute("stock", stock);		
		return "stock/moveConfirmation";
	}
	
	/*
	 * update after stock movement
	 * - back to stock list
	 */
	@GetMapping("/{id}/{id2}/confirmNewLocation")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String executeStockMovement(@PageableDefault(size = 10) Pageable pageable,
			@PathVariable String id, @PathVariable String id2, Model model) {
		log.debug("executeStockMovement - id = {}", id);
		log.debug("executeStockMovement - id2 = {}", id2);
		
		Stock stock = stockService.getStockById(new Long(id));
		Location location = locationService.getLocationById(new Long(id2));
		stock.setLoc(location);
		boolean success = stockService.saveStock(stock) != null;
		String successString = success ? "was succcessful" : "failed";
		log.debug("executeStockMovement - save method {}", successString);
		
		model.addAttribute("stockSearch", new StockSearchTO());
		model.addAttribute("lastSearch", new StockSearchTO());
		model.addAttribute("page", stockService.getStockSearchResult("", false, null, null, null, pageable));		
		return "stock/list";
	}
	
	/*
	 * stock correction
	 * 1st step: open form to enter the new amount
	 */
	@GetMapping("/{id}/correct")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showStockCorrectionForm(@PathVariable String id, Model model) {
		log.debug("showStockCorrectionForm - id = {}", id);
		
		Stock stock = stockService.getStockById(new Long(id));
		StockCorrectionTO stockCorrection = new StockCorrectionTO(stock);
		model.addAttribute("stockCorrection", stockCorrection);
		return "stock/stockCorrectionForm";
	}
	
	/*
	 * stock correction
	 * 2nd step: booking the correction, logging
	 * if necessary, delete stock
	 */
	@PostMapping("/executeCorrection")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String executeStockCorrection(@ModelAttribute StockCorrectionTO stockCorrection) {
		log.debug("executeStockCorrection - stock.id = {}", stockCorrection.getId());
		Integer newUnitCount = stockCorrection.getUnits();
		
		Stock savedStock = stockService.getStockById(stockCorrection.getId());
		Integer oldUnitCount = savedStock.getUnits();

		log.debug("executeStockCorrection - old count = {}", oldUnitCount);
		log.debug("executeStockCorrection - new count = {}", newUnitCount);
		
		if(oldUnitCount.equals(newUnitCount)) {
			log.debug("unit count not changed - nothing to do!");
		}
		else {
			boolean deleteStock = false;
			if(newUnitCount < 1) {
				deleteStock = true;
			}

			/*
			 *  log stock correction
			 *  could also be implemented with a
			 *  stock correction table
			 */
			log.warn("*** stock correction ***");
			log.warn("*** stock correction: stock id {}, product {}, location {}", 
					savedStock.getId(), 
					savedStock.getProduct().getProductNo(), 
					savedStock.getLoc().getCoordinates());
			log.warn("*** stock correction: old unit count {}, new unit count{}", 
					oldUnitCount, newUnitCount);
			if(deleteStock) {
				log.warn("*** stock correction: stock will be deleted");
			}
			log.warn("*** stock correction ***");
			
			if(deleteStock == false) {
				savedStock.setUnits(newUnitCount);
				stockService.saveStock(savedStock);
			}
			else {
				stockService.deleteStock(savedStock);
			}
		}

		return "redirect:/stock/list";
	}
	
	
}
