package info.stieghorst.simpleWMS.controller;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import info.stieghorst.simpleWMS.domain.Location;
import info.stieghorst.simpleWMS.service.LocationService;
import info.stieghorst.simpleWMS.transfer.LocationSearchTO;
import info.stieghorst.simpleWMS.transfer.LocationTO;
import lombok.extern.slf4j.Slf4j;

/*
 * controller to handle all location-related pages
 */
@Slf4j
@Controller
@RequestMapping("/location")
public class LocationController {
	
	private final LocationService locationService;
	
	public LocationController(LocationService locationService) {
		this.locationService = locationService;
	}

	/*
	 * list all locations
	 * for the give search criteria
	 * 
	 * pagination available
	 * 
	 */
	@RequestMapping("/list")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showLocationSearchResults(
			@PageableDefault(size = 10) Pageable pageable,
			@Valid @ModelAttribute("locationSearch") LocationSearchTO locationSearch, 
			BindingResult bindingResult,
			Model model) {
		log.debug("showLocationSearchResults - start");
		
		if(bindingResult.hasErrors()) {
			log.error("errors in search criteria");
			for (Object object : bindingResult.getAllErrors()) {
			    if(object instanceof FieldError) {
			        FieldError fieldError = (FieldError) object;
			        log.error("FieldError: {}/{}", fieldError.getField(), fieldError.getCode());
			    }
			    if(object instanceof ObjectError) {
			        ObjectError objectError = (ObjectError) object;
			        log.error("ObjectError: {}/{}", objectError.getObjectName(), objectError.getCode());
			    }
			}
			model.addAttribute("locationSearch", locationSearch);
			model.addAttribute("lastSearch", new LocationSearchTO());
			model.addAttribute("page", locationService.getLocationSearchResult(new LocationSearchTO(), pageable));		
			return "location/list";
		}
		
		if(locationSearch.getLocked() != null && !locationSearch.getLocked().equals("egal")) {
			locationSearch.setNoStock(false);
			locationSearch.setAisle(null);
			locationSearch.setStack(null);
		}
		else if(locationSearch.isNoStock()) {
			locationSearch.setAisle(null);
			locationSearch.setStack(null);
		}
		model.addAttribute("lastSearch", locationSearch);
		model.addAttribute("page", locationService.getLocationSearchResult(locationSearch, pageable));
		
		return "location/list";
	}
	
	/*
	 * list "all" locations matching the given ID
	 * will only be one, but shown in the paginated list
	 */
	@GetMapping("searchById/{id}/")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showLocationSearchResultById(@PageableDefault(size = 10) Pageable pageable,
			@PathVariable String id, Model model) {
		log.debug("showLocationSearchResultById - id = {}", id);
		
		LocationSearchTO lastSearch = new LocationSearchTO();
		lastSearch.setId(new Long(id));

		model.addAttribute("locationSearch", new LocationSearchTO());
		model.addAttribute("lastSearch", new LocationSearchTO());
		model.addAttribute("page", locationService.getLocationSearchResult(lastSearch, pageable));		
		
		return "location/list";
	}
	
	/*
	 * show an existing location
	 * using the form template
	 */
	@GetMapping("/{id}/show")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showById(@PathVariable String id, Model model) {
		log.debug("showById - id = {}", id);
		
		Location location = locationService.getLocationById(new Long(id));
		LocationTO transferLocation = new LocationTO(location);
		transferLocation.setEditDisabled(true);
		transferLocation.setTransferMode("show");

		model.addAttribute("location", transferLocation);
		return "location/form";
	}
	
	/*
	 * show an existing location
	 * to be edited in the form template
	 */
	@GetMapping("/{id}/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String editById(@PathVariable String id, Model model) {
		log.debug("editById - id = {}", id);
		
		Location location = locationService.getLocationById(new Long(id));
		LocationTO transferLocation = new LocationTO(location);
		transferLocation.setEditDisabled(false);
		transferLocation.setTransferMode("edit");

		model.addAttribute("location", transferLocation);
		return "location/form";
	}
	
	/*
	 * open the form template
	 * to create a new location
	 */
	@GetMapping("/new")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String newLocation(Model model) {
		log.debug("newLocation");
		
		Location location = new Location();
		LocationTO transferLocation = new LocationTO(location);
		transferLocation.setEditDisabled(false);
		transferLocation.setTransferMode("new");

		model.addAttribute("location", transferLocation);
		return "location/form";
	}
	
	/*
	 * open template to create a new aisle
	 */
	@GetMapping("/newAisle")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String newAisle(Model model) {
		log.debug("newAisle");

		Location location = new Location();
		LocationTO transferLocation = new LocationTO(location);
		transferLocation.setEditDisabled(false);
		transferLocation.setTransferMode("newAisle");

		model.addAttribute("location", transferLocation);
		return "location/form";
	}
	
	/*
	 * open template to create a new aisle
	 */
	@GetMapping("/newStack")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String newStack(Model model) {
		log.debug("newStack");

		Location location = new Location();
		LocationTO transferLocation = new LocationTO(location);
		transferLocation.setEditDisabled(false);
		transferLocation.setTransferMode("newStack");

		model.addAttribute("location", transferLocation);
		return "location/form";
	}
	
	/*
	 * show an existing location
	 * in the form template
	 * to let the user confirm
	 * that it should be deleted
	 * 
	 * not used anymore
	 * TODO remove
	 */
	@GetMapping("/{id}/deleteForm")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteFormById(@PathVariable String id, Model model) {
		log.debug("deleteFormById - id = {}", id);
		
		Location location = locationService.getLocationById(new Long(id));
		LocationTO transferLocation = new LocationTO(location);
		transferLocation.setEditDisabled(true);
		transferLocation.setTransferMode("delete");

		model.addAttribute("location", transferLocation);
		return "location/form";
	}
	
	/*
	 * called by the delete button 
	 * in the form template
	 * calls the service to delete the location
	 * and then redirects to the location list
	 * 
	 * not used anymore
	 * TODO remove
	 */
	@GetMapping("/{id}/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteById(@PathVariable String id) {
		log.debug("deleteById - id = {}", id);
		
		try {
			locationService.deleteLocationById(new Long(id));
		} catch(Exception e) {
			log.error("exception: {}", e.getLocalizedMessage());
			return "error";
		}
		
		return "redirect:/location/list";
	}
	
	/*
	 * called by the save button
	 * in the form template
	 * calls the location service to 
	 * update or insert the location
	 * and then redirects to the location list
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveOrUpdateLocation(@ModelAttribute LocationTO transferLocation) {
		log.debug("saveOrUpdateLocation - transferMode returned = {}", transferLocation.getTransferMode());
		log.debug("saveOrUpdateLocation - location coordinates returned = {}", transferLocation.getCoordinates());

		boolean success;
		Integer newAisle;
		Integer newStack; 
		Integer newLevel;
		Integer noOfStacks; 
		Integer noOfLevels;
		String transferMode = transferLocation.getTransferMode();
		switch(transferMode) {
		case "show":
			// transferMode "show" - nothing to do
			break;
		case "newAisle":
			// coordinates entered = max coordinates in new aisle
			newAisle = transferLocation.getAisle();
			noOfStacks = transferLocation.getStack(); 
			noOfLevels = transferLocation.getLevel();
			success = locationService.createNewAisle(newAisle, noOfStacks, noOfLevels); 
			break;
		case "newStack":
			// coordinates entered = max coordinates in new stack
			newAisle = transferLocation.getAisle();
			newStack = transferLocation.getStack(); 
			noOfLevels = transferLocation.getLevel();
			success = locationService.createNewStack(newAisle, newStack, noOfLevels); 
			break;
		case "new":
			newAisle = transferLocation.getAisle();
			newStack = transferLocation.getStack(); 
			newLevel = transferLocation.getLevel();
			success = locationService.createNewLocation(newAisle, newStack, newLevel);
			break;
		case "edit":
			log.debug("saveOrUpdateLocation, id given = {}", transferLocation.getId());
			success = locationService.saveLocation(transferLocation.getLocation()) != null;
			if(success) {
				log.info("saveOrUpdateLocation, location saved successfully");	
			}
			else {
				log.error("saveOrUpdateLocation, saving location failed!!!");	
			}
			break;
		}
		
		return "redirect:/location/list";
	}
	


}
