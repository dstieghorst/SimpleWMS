package info.stieghorst.simpleWMS.controller;

import java.security.Principal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;
import info.stieghorst.simpleWMS.domain.Product;
import info.stieghorst.simpleWMS.domain.Stock;
import info.stieghorst.simpleWMS.domain.User;
import info.stieghorst.simpleWMS.service.PickingService;
import info.stieghorst.simpleWMS.service.ProductService;
import info.stieghorst.simpleWMS.service.StockService;
import info.stieghorst.simpleWMS.service.UserService;
import info.stieghorst.simpleWMS.transfer.NewPickingItemTO;
import info.stieghorst.simpleWMS.transfer.NewPickingOrderTO;
import info.stieghorst.simpleWMS.transfer.PickingItemTO;
import info.stieghorst.simpleWMS.transfer.PickingOrderSearchTO;
import lombok.extern.slf4j.Slf4j;

/*
 * controller to handle all picking-related pages
 */
@Slf4j
@Controller
@RequestMapping("/picking")
public class PickingController {

	private final PickingService pickingService;
	
	private final ProductService productService;
	
	private final StockService stockService;
	
	private final UserService userService;

	public PickingController(PickingService pickingService, ProductService productService, StockService stockService, UserService userService) {
		this.pickingService = pickingService;
		this.productService = productService;
		this.stockService = stockService;
		this.userService = userService;
	}
	
	/*
	 * list all picking orders
	 * matching the current search criteria
	 */
	@RequestMapping("/list")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showPickingOrderSearchResults(
			@PageableDefault(size = 5) Pageable pageable,
			@Valid @ModelAttribute("orderSearch") PickingOrderSearchTO orderSearch, 
			Principal principal,
			Model model) {

		log.debug("showPickingOrderSearchResults - start");
		log.debug("created: {}", orderSearch.getCreated());
		
		String created = orderSearch.getCreated();
		Date dateCreated = null;
		if(created != null && created.trim().length() > 0) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				dateCreated = dateFormat.parse(created.trim());
			} catch (ParseException e) {
				dateCreated = null;
				orderSearch.setCreated("");
			}
		}
		String username = principal.getName();
		if(orderSearch.getAssignment() != null && !orderSearch.getAssignment().equals("all")) {
			orderSearch.setCreated("");
			orderSearch.setOrderStatus("");
		}
		else if(orderSearch.getCreated() != null && orderSearch.getCreated().trim().length() > 0) {
			orderSearch.setOrderStatus("");
		}
		model.addAttribute("lastSearch", orderSearch);
		model.addAttribute("page", pickingService.getPickingOrderSearchResults(
				dateCreated, orderSearch.getOrderStatus(),
				username, orderSearch.getAssignment(), pageable));		
		
		return "picking/list";
	}
	
	/*
	 * list all existing picking orders
	 */
	@RequestMapping("{id}/show")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showPickingOrderDetails(@PathVariable String id, Model model, Principal principal) {
		log.debug("showPickingOrderDetails - start");
		
		PickingOrder order = pickingService.getOrderById(new Long(id));
		User currentUser = userService.getUserByName(principal.getName());

		boolean myOrder = false; 
		if(order.getUser() != null && currentUser != null) {
			myOrder = order.getUser().equals(currentUser);
		}
		log.debug("order is assigned to the current user: {}", myOrder);
		
		NewPickingItemTO newItem = new NewPickingItemTO(order.getId());

		model.addAttribute("order", order);
		model.addAttribute("newItem", newItem);
		model.addAttribute("myOrder", myOrder);
		
		return "picking/orderDetails";
	}
	
	/*
	 * show form to create a new picking order
	 */
	@RequestMapping("/createOrder")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showCreateOrderForm(Model model) {
		
		model.addAttribute("newOrder", new NewPickingOrderTO());
		return "/picking/newOrder";
	}
	
	/*
	 * save a new picking order
	 */
	@RequestMapping("/new")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveCreatedOrder(@ModelAttribute("newOrder") NewPickingOrderTO newOrder, Model model) {
		log.debug("saveCreatedOrder - start");
		log.debug("orderName: {}", newOrder.getOrderName());
		
		PickingOrder order = new PickingOrder(newOrder.getOrderName());
		order = pickingService.savePickingOrder(order);
		
		return "redirect:/picking/" + order.getId() + "/show";
	}
	
	/*
	 * change status of a picking order
	 */
	@RequestMapping("{id}/changeStatus")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String changePickingOrderStatus(@PathVariable String id, Model model) {
		log.debug("changePickingOrderStatus - start");
		
		PickingOrder order = pickingService.getOrderById(new Long(id));
		order.changeStatus();
		pickingService.savePickingOrder(order);
		
		return "redirect:/picking/" + order.getId() + "/show";
	}
	
	/*
	 * assing picking order to a user
	 */
	@RequestMapping("{id}/assign")
	public String assignPickingOrder(@PathVariable String id, Model model, Principal principal) {
		log.debug("assignPickingOrder - start");
		log.debug("username = {}", principal.getName());
		
		PickingOrder order = pickingService.getOrderById(new Long(id));
		User currentUser = userService.getUserByName(principal.getName());

		if(order.getUser() != null) {
			log.debug("user already assigned! - no change");
		}
		else {
			if(currentUser != null) {
				order.setUser(currentUser);
				order.setOrderStatus(PickingOrder.STATUS_IN_PROGRESS);
				pickingService.savePickingOrder(order);
			}
		}

		return "redirect:/picking/" + order.getId() + "/show";
	}
	
	/*
	 * get data to edit the items of a picking order
	 */
	@RequestMapping("{id}/manageItems")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showItemManagementPage(@PathVariable String id, Model model) {
		log.debug("showItemManagementPage - start");
		
		PickingOrder order = pickingService.getOrderById(new Long(id));
		
		List<Product> existingProducts = new ArrayList<>();
		for(PickingItem item : order.getItems()) {
			existingProducts.add(item.getProduct());
		}
		
		log.debug("existingProducts");
		for(Product product : existingProducts) {
			log.debug(product.getProductNo());
		}
		
		List<Product> availableProducts = productService.getAvailableProducts();
		log.debug("availableProducts - before removing");
		for(Product product : availableProducts) {
			log.debug(product.getProductNo());
		}

		availableProducts.removeAll(existingProducts);
		log.debug("availableProducts - after removing");
		for(Product product : availableProducts) {
			log.debug(product.getProductNo());
		}
		
		NewPickingItemTO newItem = new NewPickingItemTO(order.getId());
		
		model.addAttribute("order", order);
		model.addAttribute("newItem", newItem);
		model.addAttribute("availableProducts", availableProducts);
		
		return "picking/editItems";
	}
	
	/*
	 * close a picking order
	 */
	@RequestMapping("{id}/close")
	public String closePickingOrder(@PathVariable String id, Model model) {
		log.debug("closePickingOrder - start");
		
		PickingOrder order = pickingService.getOrderById(new Long(id));
		
		order.setOrderStatus(PickingOrder.STATUS_FINISHED);
		pickingService.savePickingOrder(order);
		
		return "redirect:/picking/" + order.getId() + "/show";
	}
	
	/*
	 * add an item to a picking order
	 */
	@PostMapping("/addItem")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String addNewPickingItem(
			@ModelAttribute("newItem") NewPickingItemTO newItem,
			Model model) throws Exception {
		
		log.debug("addNewPickingItem - newItem: {}", newItem.getProductNo());
		log.debug("addNewPickingItem - order: {}", newItem.getOrderId());
		
		PickingOrder order = pickingService.getOrderById(newItem.getOrderId());

		if(order == null) {
			log.error("order not found: {}", newItem.getOrderId());
			// TODO better throw an exception
			throw new Exception("Auftrag mit der ID " + newItem.getOrderId() + " nicht gefunden!");
		}
		else {
			log.debug("productNo: {}", newItem.getProductNo());
			
			Product product = productService.getProductByProductNo(newItem.getProductNo());
			
			// is this a valid product?
			if(product != null && product.isActive()) {
				List<Stock> stockList = stockService.getStockSearchResultsForProduct(product.getId());
				
				// is stock available for this product?
				if(stockList.size() > 0) {
					PickingItem item = newItem.getPickingItem(order, product);
					item = pickingService.savePickingItem(item);
					order.getItems().add(item);
					pickingService.savePickingOrder(order);
				}
				else {
					throw new Exception("Artikelnummer "+ newItem.getProductNo() + " - kein Bestand gefunden");
				}
			}
			else {
				throw new Exception("Artikelnummer "+ newItem.getProductNo() + " ungültig oder Artikel nicht aktiv");
			}
		}
		
		return "redirect:/picking/" + order.getId() + "/manageItems";
	}
	
	/*
	 * remove an item from a picking order
	 */
	@RequestMapping("{id}/removeItem/{id2}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String removeItemFromOrder(@PathVariable String id, @PathVariable String id2, Model model) {
		log.debug("PickingController - removeItemFromOrder - orderId: {}, itemId: {}", id, id2);
		
		PickingOrder order = pickingService.getOrderById(new Long(id));
		PickingItem item = pickingService.getPickingItemById(new Long(id2));
		order.getItems().remove(item);
		pickingService.savePickingOrder(order);
		pickingService.deletePickingItem(item);

		
		
		return "redirect:/picking/" + order.getId() + "/manageItems";
	}
	
	/*
	 * prepare page to edit a picking item:
	 * search for stock to associate with this item
	 */
	@RequestMapping("{id}/editItem")
	public String editPickingItem(@PathVariable String id, Model model) throws Exception {
		log.debug("PickingController - editPickingItem - id: {}", id);
		
		PickingItem pickingItem = pickingService.getPickingItemById(new Long(id));
		log.debug("picking item found: {}/{}", pickingItem.getId(), pickingItem.getOrderQuantity());
		
		Product product = pickingItem.getProduct();
		log.debug("product found: {}", product.getProductNo());
		
		PickingItemTO item = new PickingItemTO(pickingItem);

		List<Stock> availableStocks = stockService.getStockSearchResultsForProduct(product.getId());
		
		Stock stockToPick = null;
		if(availableStocks.size() == 0) {
			/*
			 *  could this happen?
			 *  availability was checked when creating the item
			 *  but in meantime stock could have been dimished
			 *  
			 *  maybe throw an exception?
			 */
			log.error("no stock available!");
			item.setStockId(null);
			item.setStockLocation("kein Lagerplatz");
			item.setActualQuantity(0);
		}
		else {
			// for now, just take the first in the list
			stockToPick = availableStocks.get(0);
			item.setStockId(stockToPick.getId());
			item.setStockLocation(stockToPick.getLoc().getCoordinates());
		}
		
		
		model.addAttribute("item", item);
		
		return "picking/editItem";
	}
	
	/*
	 * update picking item,
	 * name actual quantity
	 */
	@PostMapping("/saveItem")
	public String savePickingItem(@ModelAttribute("item") PickingItemTO item, Model model) {
		log.debug("PickingController - savePickingItem");
		log.debug("checking entered quantity");
		log.debug("item returned: {}", item);
		
		
		int pickQuantity = item.getActualQuantity();
		if(pickQuantity > item.getOrderQuantity()) {
			log.debug("actualQuantity > orderQuantity - reduced to {}", pickQuantity);
			pickQuantity = item.getOrderQuantity();
		}
		
		if(item.getStockId() != null) {
			Stock stock = stockService.getStockById(item.getStockId());
			int stockQuantity = stock.getUnits();
			if(stockQuantity < pickQuantity) {
				pickQuantity = stockQuantity;
			}
			
			stockQuantity -= pickQuantity;
			stock.setUnits(stockQuantity);
			if(stockQuantity == 0) {
				log.warn("stock.units = 0 - stock will be deleted");
				stockService.deleteStock(stock);
			}
			else {
				stockService.saveStock(stock);
			}
		}
		
		PickingItem pickingItem = pickingService.getPickingItemById(item.getId());
		pickingItem.setActualQuantity(pickQuantity);
		pickingItem.setItemStatus(PickingItem.STATUS_FINISHED);
		pickingService.savePickingItem(pickingItem);
		
		return "redirect:/picking/" + pickingItem.getOrder().getId() + "/show";
	}
	
	/*
	 * open page to edit a new picking item
	 * (new = order not yet in progress)
	 */
	@RequestMapping("{id}/editNewItem")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String editNewPickingItem(@PathVariable String id, Model model) throws Exception {
		log.debug("PickingController - editNewPickingItem - id: {}", id);
		
		PickingItem pickingItem = pickingService.getPickingItemById(new Long(id));
		log.debug("picking item found: {}/{}", pickingItem.getId(), pickingItem.getOrderQuantity());
		
		PickingItemTO item = new PickingItemTO(pickingItem);
		
		model.addAttribute("item", item);
		
		return "picking/editNewItem";
	}
	
	/*
	 * update picking item
	 * for a new order
	 */
	@PostMapping("/saveNewItem")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveNewPickingItem(@ModelAttribute("item") PickingItemTO item, Model model) {
		log.debug("PickingController - saveNewPickingItem");
		log.debug("item returned: {}", item);
		
		
		int orderQuantity = item.getOrderQuantity();
		PickingItem pickingItem = pickingService.getPickingItemById(item.getId());
		
		if(orderQuantity > 0) {
			pickingItem.setOrderQuantity(orderQuantity);
			pickingService.savePickingItem(pickingItem);
		}
		
		return "redirect:/picking/" + pickingItem.getOrder().getId() + "/manageItems";
	}
	

}
