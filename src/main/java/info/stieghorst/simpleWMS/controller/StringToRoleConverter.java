package info.stieghorst.simpleWMS.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.service.UserService;

@Component
public class StringToRoleConverter implements Converter<String, Role> {
	
	@Autowired
	UserService userService;

	@Override
	public Role convert(String source) {
		Long id = new Long(source);
		return userService.getRoleById(id);
	}

}
