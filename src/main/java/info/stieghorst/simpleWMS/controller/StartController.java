package info.stieghorst.simpleWMS.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 *  a simple controller for the start page
 */
@Controller
public class StartController {
	
	/*
	 * open the start/index page
	 */
	@RequestMapping({"/", "start"})
	public String startPage(Model model) {
		
		return "index";
	}

	/* 
	 * open the login form
	 */
	@RequestMapping({"/login"})
	public String showLoginPage(Model model) {
		
		return "login";
	}

	/*
	 * open logout confirmation
	 * (logout is handled by spring security)
	 */
	@GetMapping({"/logout-success"})
	public String showLogoutPage(Model model) {
		
		return "logout";
	}
}
