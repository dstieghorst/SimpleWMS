package info.stieghorst.simpleWMS.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;
import info.stieghorst.simpleWMS.service.UserService;
import info.stieghorst.simpleWMS.transfer.UserPasswordTO;
import lombok.extern.slf4j.Slf4j;

/*
 * controller to handle all user-related pages
 */
@Slf4j
@Controller
@RequestMapping("/user")
public class UserController {
	
	private final UserService userService;
	
	private PasswordEncoder encoder;

	public UserController(UserService userService) {
		this.userService = userService;
		this.encoder = new BCryptPasswordEncoder(11);
	}
	
	/*
	 * prepare data for password form
	 */
	@RequestMapping("/passwordChange")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showPasswordForm(Principal principal, Model model) {
		log.debug("showPasswordForm - user: " + principal.getName());
		
		String userName = principal.getName();
		User user = userService.getUserByName(userName);
		UserPasswordTO transferUser = new UserPasswordTO(user);
		
		List<String> errors = new ArrayList<>();
		
		model.addAttribute("userDetails", transferUser);
		model.addAttribute("errors", errors);
		
		return "user/changePassword";
		
	}
	
	/*
	 * process password form
	 */
	@PostMapping("/savePassword")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String processPasswordChange(
			@ModelAttribute("userDetails") UserPasswordTO transferUser, Model model) {
		log.debug("processPasswordChange - user: " + transferUser.getUsername());
		
		List<String> errors = new ArrayList<>();
		
		User user = userService.getUserByName(transferUser.getUsername());
		
		String oldPasswordInput = transferUser.getOldPasswordInput();
		String newPasswordInput1 = transferUser.getNewPassword1();
		String newPasswordInput2 = transferUser.getNewPassword2();
		
		if(!encoder.matches(oldPasswordInput, user.getPassword())) {
			log.debug("Altes Passwort falsch eingegeben!");
			errors.add("Altes Passwort falsch eingegeben!");
		}
		
		if(newPasswordInput1 == null || newPasswordInput1.length() < 6) {
			log.debug("neues Passwort zu kurz");
			errors.add("Das neue Passwort muss mindestens 6 Zeichen haben!");
		}
		
		if(!newPasswordInput1.equals(newPasswordInput2)) {
			log.debug("Neues Password muss zweimal gleich eingegeben werden!");
			errors.add("Neues Password muss zweimal gleich eingegeben werden!");
		}
		
		if(errors.size() > 0) {
			transferUser = new UserPasswordTO(user);
			
			model.addAttribute("userDetails", transferUser);
			model.addAttribute("errors", errors);
			
			return "user/changePassword";
		}
		else {
			userService.saveUser(user, encoder.encode(newPasswordInput1));
			
			return "user/passwordChanged";
		}
	}
	
	/*
	 * show the list of all user roles
	 */
	@RequestMapping("/userRoles")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showRoleList(Model model) {
		log.debug("showRoleList");
		
		Role selectedRole = new Role();
		List<Role> completeRoleList = userService.getCompleteRoleList();
		
		for(Role role : completeRoleList) {
			log.debug("search users with role {}", role);
			List<User> userList = userService.getUsersIncludingRole(role);
			log.debug("no of users found: {}", userList.size());
			Set<User> userSet = new HashSet<>(userList);
			role.setUsers(userSet);
		}
		
		model.addAttribute("roles", completeRoleList);
		model.addAttribute("selectedRole", selectedRole);
		return "user/roleList";
	}

	/*
	 * user has selected a certain role to be edited
	 * - show this role and also the complete list
	 */
	@RequestMapping("/userRoles/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showRoleListAndSelectedRole(@PathVariable String id, Model model) {
		log.debug("showRoleListAndSelectedRole - id = {}", id);
		
		Role selectedRole = userService.getRoleById(new Long(id));
		List<Role> completeRoleList = userService.getCompleteRoleList();
		for(Role role : completeRoleList) {
			log.debug("search users with role {}", role);
			List<User> userList = userService.getUsersIncludingRole(role);
			log.debug("no of users found: {}", userList.size());
			Set<User> userSet = new HashSet<>(userList);
			role.setUsers(userSet);
		}
		
		model.addAttribute("roles", completeRoleList);
		model.addAttribute("selectedRole", selectedRole);
		return "user/roleList";
	}
	
	/*
	 * process creating or updating a user role
	 */
	@PostMapping("/saveRole")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveOrUpdateRole(@ModelAttribute Role selectedRole) {
		log.debug("saveOrUpdateRole");
		log.debug("selectedRole: {}/{}", selectedRole.getId(), selectedRole.getRoleName());
		
		selectedRole.setRoleName(selectedRole.getRoleName().toUpperCase());
		
		if(selectedRole.getId() == null) {
			userService.saveNewRole(selectedRole);
		}
		else {
			userService.saveRole(selectedRole);
		}
		
		return "redirect:/user/userRoles";
	}
	
	/*
	 * delete a user role and go back to the list
	 */
	@GetMapping("/deleteRole/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteRoleById(@PathVariable String id) {
		log.debug("deleteRoleById - id = {}", id);
		
		try {
			userService.deleteRoleById(new Long(id));
		} catch(Exception e) {
			log.error("exception: {}", e.getLocalizedMessage());
			return "error";
		}
		
		return "redirect:/user/userRoles";
		
	}

	/*
	 * show list of all users in the system
	 */
	@RequestMapping("/userList")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showUserList(Model model) {
		log.debug("showUserList");
		
		User selectedUser = new User();
		List<User> completeUserList = userService.getCompleteUserList();
		
		model.addAttribute("selectedUser", selectedUser);
		model.addAttribute("users", completeUserList);
		return "user/userList";
	}

	/*
	 * show list of all user
	 * and also a single user
	 * the user wants to changed
	 */
	@RequestMapping("/userList/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showUserListAndSelectedUser(@PathVariable String id, Model model) {
		log.debug("showUserListAndSelectedUser - id = {}", id);
		
		User selectedUser = userService.getUserById(new Long(id));
		List<User> completeUserList = userService.getCompleteUserList();
		List<Role> completeRoleList = userService.getCompleteRoleList();
		
		model.addAttribute("selectedUser", selectedUser);
		model.addAttribute("users", completeUserList);
		model.addAttribute("allRoles", completeRoleList);
		return "user/userList";
	}

	/*
	 * save a newly created or updated user
	 */
	@PostMapping("/saveUser")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveOrUpdateUser(@ModelAttribute User selectedUser) {
		log.debug("saveOrUpdateUser");
		log.debug("selectedUser: {}/{}", selectedUser.getId(), selectedUser.getUsername());
		
		if(selectedUser.getId() == null) {
			// set standard password for new user
			String password = encoder.encode("password");
			selectedUser.setPassword(password);
			userService.saveNewUser(selectedUser);
		}
		else {
			userService.saveUser(selectedUser);
		}
		
		return "redirect:/user/userList";
	}
	
	/*
	 * delete the selected user
	 */
	@GetMapping("/deleteUser/{id}")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteuserById(@PathVariable String id) {
		log.debug("deleteUserById - id = {}", id);
		
		try {
			userService.deleteUserById(new Long(id));
		} catch(Exception e) {
			log.error("exception: {}", e.getLocalizedMessage());
			return "error";
		}
		
		return "redirect:/user/userList";
		
	}

}
