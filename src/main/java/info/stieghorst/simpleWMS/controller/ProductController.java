package info.stieghorst.simpleWMS.controller;

import javax.validation.Valid;

import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import info.stieghorst.simpleWMS.domain.Product;
import info.stieghorst.simpleWMS.service.ProductService;
import info.stieghorst.simpleWMS.transfer.ProductTO;
import info.stieghorst.simpleWMS.transfer.ProductSearchTO;
import lombok.extern.slf4j.Slf4j;

/*
 * controller to handle all product-related pages
 */
@Slf4j
@Controller
@RequestMapping("/product")
public class ProductController {
	
	private final ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	/*
	 * used in connect with search fields
	 * null is allowed, but empty field is string with length 0,
	 * therefore triggering @Size error
	 * this binder converts empty string to null
	 */
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
	}
	
	/*
	 * list all products
	 * or all those
	 * matching the given search criteria
	 * 
	 * using pagination
	 * 
	 * validation for seach criteria enabled
	 */
	@RequestMapping("/list")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showProductSearchResults(
			@PageableDefault(size = 10) Pageable pageable,
			@Valid @ModelAttribute("productSearch") ProductSearchTO productSearch, 
			BindingResult bindingResult,
			Model model) {
		
		log.debug("showProductSearchResults - start");
		
		if(bindingResult.hasErrors()) {
			log.error("errors in search criteria");
			for (Object object : bindingResult.getAllErrors()) {
			    if(object instanceof FieldError) {
			        FieldError fieldError = (FieldError) object;
			        log.error("FieldError: {}/{}", fieldError.getField(), fieldError.getCode());
			    }
			    if(object instanceof ObjectError) {
			        ObjectError objectError = (ObjectError) object;
			        log.error("ObjectError: {}/{}", objectError.getObjectName(), objectError.getCode());
			    }
			}
			model.addAttribute("productSearch", productSearch);
			model.addAttribute("lastSearch", new ProductSearchTO());
			Page<Product> page = productService.getProductSearchResult("", "", "", pageable);
			model.addAttribute("page", page);		
			return "product/list";
		}
		
		if(productSearch.getProductNo() != null) {
			productSearch.setName(null);
			productSearch.setDescription(null);
		}
		else if(productSearch.getName() != null) {
			productSearch.setDescription(null);
		}
		model.addAttribute("lastSearch", productSearch);
		Page<Product> page = productService.getProductSearchResult(
				productSearch.getProductNo(),
				productSearch.getName(),
				productSearch.getDescription(), 
				pageable);
		model.addAttribute("page", page);		
		
		return "product/list";
	}
	
	/*
	 * show an existing product
	 * using the form template
	 */
	@GetMapping("/{id}/show")
	@PreAuthorize("hasRole('ROLE_STANDARD')")
	public String showById(@PathVariable String id, Model model) {
		log.debug("showById - id = {}", id);
		
		Product product = productService.getProductById(new Long(id));
		ProductTO transferProduct = new ProductTO(product);
		transferProduct.setEditDisabled(true);
		transferProduct.setTransferMode("show");

		model.addAttribute("product", transferProduct);
		return "product/form";
	}
	
	/*
	 * show an existing product
	 * to be edited in the form template
	 */
	@GetMapping("/{id}/edit")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String editById(@PathVariable String id, Model model) {
		log.debug("editById - id = {}", id);
		
		Product product = productService.getProductById(new Long(id));
		ProductTO transferProduct = new ProductTO(product);
		transferProduct.setEditDisabled(false);
		transferProduct.setTransferMode("edit");

		model.addAttribute("product", transferProduct);
		return "product/form";
	}
	
	/*
	 * open the form template
	 * to create a new product
	 */
	@GetMapping("/new")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String newProduct(Model model) {
		log.debug("newProduct");
		
		Product product = new Product();
		ProductTO transferProduct = new ProductTO(product);
		transferProduct.setEditDisabled(false);
		transferProduct.setTransferMode("new");

		model.addAttribute("product", transferProduct);
		return "product/form";
	}
	
	/*
	 * show an existing product
	 * in the form template
	 * to let the user confirm
	 * that it should be deleted
	 */
	@GetMapping("/{id}/deleteForm")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteFormById(@PathVariable String id, Model model) {
		log.debug("deleteFormById - id = {}", id);
		
		Product product = productService.getProductById(new Long(id));
		ProductTO transferProduct = new ProductTO(product);
		transferProduct.setEditDisabled(true);
		transferProduct.setTransferMode("delete");

		model.addAttribute("product", transferProduct);
		return "product/form";
	}
	
	/*
	 * called by the delete button 
	 * in the form template
	 * calls the service to delete the product
	 * and then redirects to the product list
	 */
	@GetMapping("/{id}/delete")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String deleteById(@PathVariable String id) {
		log.debug("deleteById - id = {}", id);
		
		try {
			productService.deleteProductById(new Long(id));
		} catch(Exception e) {
			log.error("exception: {}", e.getLocalizedMessage());
			return "error";
		}
		
		return "redirect:/product/list";
	}
	
	/*
	 * called by the save button
	 * in the form template
	 * calls the product service to 
	 * update or insert the product
	 * and then redirects to the product list
	 */
	@PostMapping("/save")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveOrUpdateProduct(@ModelAttribute ProductTO transferProduct) {
		log.debug("saveOrUpdateProduct - transferMode returned = {}", transferProduct.getTransferMode());
		log.debug("saveOrUpdateProduct - transferProduct: {}", transferProduct);
		log.debug("saveOrUpdateProduct - product returned = {}", transferProduct.getProductNo());
		
		if(!transferProduct.getTransferMode().equals("show")) {
			log.debug("saveOrUpdateProduct, id given = {}", transferProduct.getId());
			boolean success = productService.saveProduct(transferProduct.getProduct()) != null;
			if(success) {
				log.info("saveOrUpdateProduct, product saved successfully");	
			}
			else {
				log.error("saveOrUpdateProduct, saving product failed!!!");	
			}
		}
		
		return "redirect:/product/list";
	}
	



	
	

}
