package info.stieghorst.simpleWMS.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;
import lombok.extern.slf4j.Slf4j;

/* 
 * service for all user operations
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
	
	@Value("${backend.url}")
    private String backendUrl;

    private final RestTemplate restTemplate = new RestTemplate();

	// user methods

	@Override
	public List<User> getCompleteUserList() {
		log.debug("getCompleteUserList - START");
		
		String url = backendUrl + "/users";
		log.debug("URL: {}", url);
		
        List<User> userList = new ArrayList<>();
		
		ResponseEntity<User[]> response = restTemplate.getForEntity(url, User[].class);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	User[] users = response.getBody();
        	userList = new ArrayList<>(Arrays.asList(users));
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
        
        return userList;
	}
	
	@Override
	public List<User> getUsersIncludingRole(Role role) {
		log.debug("getUsersIncludingRole - START");
		
		String url = backendUrl + "/users?roleName=" + role.getRoleName();
		log.debug("URL: {}", url);
		
        List<User> userList = new ArrayList<>();
		
		ResponseEntity<User[]> response = restTemplate.getForEntity(url, User[].class);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	User[] users = response.getBody();
        	userList = new ArrayList<>(Arrays.asList(users));
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
        
        return userList;
	}

	@Override
	public User getUserById(Long id) {
		log.debug("getUserById - START - id: {}", id);
		
		String url = backendUrl + "/users/{id}";
		log.debug("URL: {}", url);
		
		Map<String, String> params = new HashMap<String, String>();
	    params.put("id", id.toString());
	    
	    User user = null;
	    
	    ResponseEntity<User> response = restTemplate.getForEntity(url, User.class, params);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	user = response.getBody();
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
	    
		return user;
	}

	@Override
	public User getUserByName(String userName) {
		log.debug("getUserByName - START");
		
		String url = backendUrl + "/users?username=" + userName;
		log.debug("URL: {}", url);
		
        User user = null;
        
		ResponseEntity<User[]> response = restTemplate.getForEntity(url, User[].class);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	User[] users = response.getBody();
            if(users.length > 0) {
            	user = users[0];
            }
            else {
            	log.warn("no user found!");
            }
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
        
        return user;
	}
	
	@Override
	public User saveNewUser(User user) {
		log.debug("saveNewUser - START");
		
		String url = backendUrl + "/users";
		log.debug("POST new User - URL: {}", url);
		
		User savedUser = null;
		
		ResponseEntity<User> response = restTemplate.postForEntity(url, user, User.class);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	savedUser = response.getBody();
    		log.debug("new user saved: {}", savedUser);
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
		
		return savedUser;
	}

	@Override
	public void saveUser(User user) {
		log.debug("saveUser - START");
		
		if(user.getId() == null) {
			// new User object
			log.warn("new User - wrong method - nothing saved!");
		}
		else {
			// existing User object
			String baseUrl = backendUrl + "/users/" + user.getId().toString();
			log.debug("GET existing user - URL: {}", baseUrl);

		    User existingUser;
			try {
				existingUser = restTemplate.getForObject(baseUrl, User.class);
				String roleBaseUrl = baseUrl + "/roles/";

				for(Role role : existingUser.getRoles()) {
					String url = roleBaseUrl + role.getId().toString();
					log.debug("DELETE role for existing user - URL: {}", url);
			    	restTemplate.delete(url, existingUser);
				}
				
			    for(Role role : user.getRoles()) {
					String url = roleBaseUrl + role.getId().toString();
					log.debug("PUT new role for user - URL: {}", url);
			    	restTemplate.put(url, user);
			    }
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
			
		}
		
	}

	@Override
	public void saveUser(User user, String password) {
		log.debug("saveUser - START");
		
		if(user.getId() == null) {
			// new User object
			log.warn("new User - wrong method - nothing saved!");
		}
		else {
			// existing User object
		    user.setPassword(password);

			String url = backendUrl + "/users/" + user.getId().toString();
			log.debug("PUT existing user - URL: {}", url);
			
		    try {
				restTemplate.put(url, user);
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}
	}

	@Override
	public void deleteUserById(Long id) {
		log.debug("deleteUserById - START - id: {}", id);
		
		String url = backendUrl + "/users/" + id.toString();
		log.debug("URL: {}", url);
		
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}
	
	// role methods

	@Override
	public List<Role> getCompleteRoleList() {
		log.debug("getCompleteRoleList - START");
		
		String url = backendUrl + "/roles";
		log.debug("URL: {}", url);
		
		List<Role> roleList = new ArrayList<>();
		
		ResponseEntity<Role[]> response = restTemplate.getForEntity(url, Role[].class);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	Role[] roles = response.getBody();
        	roleList = Arrays.asList(roles);
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
		
        return roleList;
	}

	@Override
	public Role getRoleById(Long id) {
		log.debug("getRoleById - START - id: {}", id);
		
		String url = backendUrl + "/roles/" + id.toString();
		log.debug("URL: {}", url);
		
		Role role = null;
		
		ResponseEntity<Role> response = restTemplate.getForEntity(url, Role.class);
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	role = response.getBody();
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }
		
		return role;
	}

	@Override
	public Role saveNewRole(Role role) {
		log.debug("saveNewRole - START");
		
		String url = backendUrl + "/roles";
		log.debug("POST new Role - URL: {}", url);
		
		Role savedRole = null;
		
		ResponseEntity<Role> response = restTemplate.postForEntity(url, role, Role.class); 
        if(response.getStatusCode().equals(HttpStatus.OK)) {
        	savedRole = response.getBody();
    		log.debug("new role saved: {}", savedRole.toString());
        }
        else {
        	log.error("REST request status code: {}", response.getStatusCode());
        }

		return savedRole;
	}

	@Override
	public void saveRole(Role role) {
		log.debug("saveRole - START");
		
		if(role.getId() == null) {
			// new Role object
			log.warn("new Role - wrong method - nothing saved!");
		}
		else {
			String url = backendUrl + "/roles/" + role.getId().toString();
			log.debug("PUT existing role - URL: {}", url);
			
			try {
				restTemplate.put(url, role);
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}
		
	}

	@Override
	public void deleteRoleById(Long id) {
		log.debug("deleteRoleById - START - id: {}", id);
		
		String url = backendUrl + "/roles/" + id.toString();
		log.debug("URL: {}", url);
		
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}

}
