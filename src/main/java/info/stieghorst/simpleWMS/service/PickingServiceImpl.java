package info.stieghorst.simpleWMS.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;
import info.stieghorst.simpleWMS.transfer.RestPage;
import lombok.extern.slf4j.Slf4j;

/* 
 * service for all picking operations
 */
@Slf4j
@Service
public class PickingServiceImpl implements PickingService {
	
	@Value("${backend.url}")
    private String backendUrl;

    private final RestTemplate restTemplate = new RestTemplate();
    
    private final ParameterizedTypeReference<RestPage<PickingOrder>> orderResponseType 
    	= new ParameterizedTypeReference<RestPage<PickingOrder>>() { };

	@Override
	public List<PickingOrder> getCompletePickingOrderList() {
		log.debug("PickingServiceImpl - getCompletePickingOrderList");
		
		String url = backendUrl + "/orders";
		log.debug("URL: {}", url);
		
		List<PickingOrder> orderList = new ArrayList<>();
		
		ResponseEntity<PickingOrder[]> response = restTemplate.getForEntity(url, PickingOrder[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			PickingOrder[] orders = response.getBody();
			orderList = new ArrayList<>(Arrays.asList(orders));
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		for(PickingOrder order : orderList) {
			log.debug("search items for order {}/{}", order.getId().toString(), order.getOrderName());
			String itemUrl = backendUrl + "/items?orderId=" + order.getId().toString();
			log.debug("URL: {}", itemUrl);

			ResponseEntity<PickingItem[]> itemResponse = restTemplate.getForEntity(itemUrl, PickingItem[].class);
			if(itemResponse.getStatusCode().equals(HttpStatus.OK)) {
				PickingItem[] items = itemResponse.getBody();
				List<PickingItem> itemList = new ArrayList<>(Arrays.asList(items));
				order.setItems(itemList);
			}
			else {
				log.error("REST request status code: {}", itemResponse.getStatusCode());
			}
		}
		
		log.debug("orderList has {} Elements", orderList.size());

		return orderList;
	}


	@Override
	public PickingOrder getOrderById(Long id) {
		log.debug("PickingServiceImpl - getOrderById - id = {}", id);
		
		String url = backendUrl + "/orders/" + id.toString();
		log.debug("URL: {}", url);
		
		PickingOrder order = null;
		ResponseEntity<PickingOrder> response = restTemplate.getForEntity(url, PickingOrder.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			order = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		if(order != null) {
			log.debug("search items for order {}/{}", order.getId().toString(), order.getOrderName());

			String itemUrl = backendUrl + "/items?orderId=" + order.getId().toString();
			log.debug("URL: {}", itemUrl);

			ResponseEntity<PickingItem[]> itemResponse = restTemplate.getForEntity(itemUrl, PickingItem[].class);
			if(itemResponse.getStatusCode().equals(HttpStatus.OK)) {
				PickingItem[] items = itemResponse.getBody();
				List<PickingItem> itemList = new ArrayList<>(Arrays.asList(items));
				order.setItems(itemList);
			}
			else {
				log.error("REST request status code: {}", itemResponse.getStatusCode());
			}
		}
		
		return order;
	}

	@Override
	public Page<PickingOrder> getPickingOrderSearchResults(Date created, String status, String username, String assignment, Pageable pageable) {
		log.debug("PickingServiceImpl - getPickingOrderSearchResults");
		log.debug("Date 'created' = '{}'", created);
		log.debug("'status' = '{}'", status);
		log.debug("'username' = '{}'", username);
		log.debug("'assignment' = '{}'", assignment);
		
		String url = backendUrl + "/paged/orders";

		Page<PickingOrder> page = null;

		if(assignment != null 
				&& assignment.trim().length() > 0 
				&& !assignment.equals("all")) {
			log.debug("use assignment as search criterion");
			switch(assignment) {
			case "own": 
				url = url + "?assigned=" + assignment.trim()
					+ "&username=" + username
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
				break;
			case "others" : 
				url = url + "?assigned=" + assignment.trim()
					+ "&username=" + username
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
				break;
			case "nobody" : 
				url = url + "?assigned=" + assignment.trim()
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
				break;
			default:
				url = url + "?pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
			}
		}
		else if(created != null) {
			log.debug("use Date 'created' for search");
			SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
			String createdString = inputFormat.format(created);
			
			url = url + "?created=" + createdString
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else if(status != null && status.trim().length() > 0) {
			log.debug("use status as search criterion");
			url = url + "?status=" + status.trim()
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else {
			log.debug("no search criteria to use - get all orders");
			url = url + "?pageNumber=" + pageable.getPageNumber() 
			+ "&pageSize=" + pageable.getPageSize();
		}

		log.debug("URL: {}", url);
		ResponseEntity<RestPage<PickingOrder>> result = restTemplate.exchange(url, HttpMethod.GET, null/*httpEntity*/, orderResponseType);
		if(result.getStatusCode().equals(HttpStatus.OK)) {
			page = result.getBody();
		}
		else {
			log.error("REST request status code: {}", result.getStatusCode());
		}
		
		
		if(page != null) {
			for(PickingOrder order : page.getContent()) {
				log.debug("search items for order {}/{}", order.getId().toString(), order.getOrderName());
				String itemUrl = backendUrl + "/items?orderId=" + order.getId().toString();
				log.debug("URL: {}", itemUrl);

				ResponseEntity<PickingItem[]> itemResponse = restTemplate.getForEntity(itemUrl, PickingItem[].class);
				if(itemResponse.getStatusCode().equals(HttpStatus.OK)) {
					PickingItem[] items = itemResponse.getBody();
					List<PickingItem> itemList = new ArrayList<>(Arrays.asList(items));
					order.setItems(itemList);
				}
				else {
					log.error("REST request status code: {}", itemResponse.getStatusCode());
				}
			}
		}

		return page;
	}


	@Override
	public PickingOrder savePickingOrder(PickingOrder order) {
		log.debug("savePickingOrder");
		
		PickingOrder savedOrder = null;
		
		if(order.getId() == null) {
			log.debug("new order");
			String url = backendUrl + "/orders";
			log.debug("URL: {}", url);
			
			ResponseEntity<PickingOrder> response = restTemplate.postForEntity(url, order, PickingOrder.class);
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				savedOrder = response.getBody();
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		}
		else {
			log.debug("existing order");
			String url = backendUrl + "/orders/" + order.getId().toString();
			log.debug("URL: {}", url);
			order.setLastChanged(new Date());

			try {
				restTemplate.put(url, order);
				savedOrder = order;
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}
		
		return savedOrder;
	}


	@Override
	public PickingItem savePickingItem(PickingItem item) {
		log.debug("savePickingItem");
		log.debug("item {}", item);
		
		PickingOrder order = item.getOrder();
		log.debug("save order");
		String url = backendUrl + "/orders/" + order.getId().toString();
		log.debug("URL: {}", url);
		order.setLastChanged(new Date());
		
		try {
			restTemplate.put(url, order);
			log.debug("item {}", item);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
		
		PickingItem savedItem = null;
		
		if(item.getId() == null) {
			log.debug("new item");
			url = backendUrl + "/orders/" + order.getId().toString() + "/items";
			log.debug("URL: {}", url);
			
			ResponseEntity<PickingItem> response = restTemplate.postForEntity(url, item, PickingItem.class);
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				savedItem = response.getBody();
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		}
		else {
			log.debug("existing item");
			url = backendUrl + "/items/" + item.getId().toString();
			log.debug("URL: {}", url);
			
			try {
				restTemplate.put(url, item);
				savedItem = item;
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}

		return savedItem;
	}


	@Override
	public PickingItem getPickingItemById(Long id) {
		log.debug("PickingServiceImpl - getPickingItemById - id = {}", id);
		
		String url = backendUrl + "/items/" + id.toString();
		log.debug("URL: {}", url);
		
		PickingItem item = null; 
				
		ResponseEntity<PickingItem> response = restTemplate.getForEntity(url, PickingItem.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			item = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		if(item != null) {
			url = backendUrl + "/orders?itemId=" + item.getId();
			log.debug("URL: {}", url);
			
			ResponseEntity<PickingOrder[]> orderResponse = restTemplate.getForEntity(url, PickingOrder[].class);
			if(orderResponse.getStatusCode().equals(HttpStatus.OK)) {
				PickingOrder[] orders = orderResponse.getBody();
				if(orders.length > 0) {
					item.setOrder(orders[0]);
				}
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		}

		return item;
	}


	@Override
	public void deletePickingItem(PickingItem item) {
		log.debug("PickingServiceImpl - deletePickingItem - id = {}", item.getId());
		
		String url = backendUrl + "/items/" + item.getId().toString();
		log.debug("URL: {}", url);
		
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}


	@Override
	public List<PickingItem> getCompletePickingItemList() {
		log.debug("PickingServiceImpl - getCompletePickingItemList");
		
		String url = backendUrl + "/items";
		log.debug("URL: {}", url);
		
		List<PickingItem> itemList = new ArrayList<>();
		
		ResponseEntity<PickingItem[]> itemResponse = restTemplate.getForEntity(url, PickingItem[].class);
		if(itemResponse.getStatusCode().equals(HttpStatus.OK)) {
			PickingItem[] items = itemResponse.getBody();
			itemList = new ArrayList<>(Arrays.asList(items));
		}
		else {
			log.error("REST request status code: {}", itemResponse.getStatusCode());
		}
		
		return itemList;
	}


	@Override
	public void deletePickingOrder(PickingOrder order) {
		log.debug("PickingServiceImpl - deletePickingOrder - id = {}", order.getId());
		
		String url = backendUrl + "/order/" + order.getId().toString();
		log.debug("URL: {}", url);
		
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
		}
	}


}
