package info.stieghorst.simpleWMS.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.stieghorst.simpleWMS.domain.Stock;

/*
 * service interface for the stock class
 */
public interface StockService {
	
	/*
	 * get all stock entries in the DB
	 */
	List<Stock> getCompleteStockList();
	
	/*
	 * get a single stock for the given ID
	 */
	Stock getStockById(Long id);

	/*
	 * get all stock entries matching the given search criteria
	 */
	Page<Stock> getStockSearchResult(String productNo, boolean noLocation, Integer aisle, Integer stack, Integer level, Pageable pageable) ;
	
	/*
	 * get all stock entries for the given product ID
	 */
	List<Stock> getStockSearchResultsForProduct(Long id);
	
	/*
	 * get all stock entries for the given location ID
	 */
	List<Stock> getStockSearchResultsForLocation(Long id);

	/*
	 * save the given stock
	 */
	Stock saveStock(Stock stock);

	/*
	 * delete the given stock
	 */
	boolean deleteStock(Stock stock);

	

}
