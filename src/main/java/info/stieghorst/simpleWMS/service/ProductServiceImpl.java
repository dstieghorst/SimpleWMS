package info.stieghorst.simpleWMS.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.Product;
import info.stieghorst.simpleWMS.domain.Stock;
import info.stieghorst.simpleWMS.transfer.RestPage;
import lombok.extern.slf4j.Slf4j;

/* 
 * service for all product operations
 */
@Slf4j
@Service
public class ProductServiceImpl implements ProductService {
	
	@Value("${backend.url}")
    private String backendUrl;

    private final RestTemplate restTemplate = new RestTemplate();
    
    private final ParameterizedTypeReference<RestPage<Product>> responseType 
    	= new ParameterizedTypeReference<RestPage<Product>>() { };

	@Override
	public List<Product> getCompleteProductList() {
		log.debug("ProductServiceImpl - getCompleteProductList");
		
		String url = backendUrl + "/products";
		log.debug("URL: {}", url);
		
		List<Product> productList = new ArrayList<>();
		
		ResponseEntity<Product[]> response = restTemplate.getForEntity(url, Product[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Product[] products = response.getBody();
			if(products.length > 0) {
				productList = new ArrayList<>(Arrays.asList(products));
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
			
		log.debug("ProductServiceImpl - productList has {} elements", productList.size());

		return productList;
	}
	
	/*
	 * find products which are active and have stock
	 * - used by PickingController in dropdown list
	 */
	public List<Product> getAvailableProducts() {
		log.debug("ProductServiceImpl - getAvailableProducts");
		
		String url = backendUrl + "/products?available=true";
		log.debug("URL: {}", url);
		
		List<Product> productList = new ArrayList<>();
		
		ResponseEntity<Product[]> response = restTemplate.getForEntity(url, Product[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Product[] products = response.getBody();
			if(products.length > 0) {
				productList = new ArrayList<>(Arrays.asList(products));
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
			
		log.debug("ProductServiceImpl - productList has {} elements", productList.size());

		return productList;
	}

	@Override
	public Page<Product> getProductSearchResult(String productNo, String name, String description, Pageable pageable) {
		log.debug("ProductServiceImpl - getProductSearchResult");
		
		String url = backendUrl + "/paged/products";

		Page<Product> page = null;

		if(productNo != null) {
			url = url + "?productNo=" + productNo 
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else if(name != null) {
			url = url + "?name=" + name 
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else if(description != null) {
			url = url + "?description=" + description 
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else {
			url += "?pageNumber=" + pageable.getPageNumber() + "&pageSize=" + pageable.getPageSize();
		}

		log.debug("URL: {}", url);
		ResponseEntity<RestPage<Product>> result = restTemplate.exchange(url, HttpMethod.GET, null/*httpEntity*/, responseType);
		if(result.getStatusCode().equals(HttpStatus.OK)) {
			page = result.getBody();
		}
		else {
			log.error("REST request status code: {}", result.getStatusCode());
		}
		
		for(Product product : page.getContent()) {
			log.debug("searching stocks for product {}/{}", product.getId(), product.getProductNo());

			String stockUrl = backendUrl + "/stocks?productId=" + product.getId().toString();
			log.debug("URL: {}", stockUrl);
			
			ResponseEntity<Stock[]> response = restTemplate.getForEntity(stockUrl, Stock[].class);
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				Stock[] stockArray = response.getBody();
				if (stockArray.length > 0) {
					product.setStocks(new ArrayList<>(Arrays.asList(stockArray)));
				}
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		}
		
		log.debug("ProductServiceImpl - productList has {} elements", page.getContent().size());

		return page;
	}


	@Override
	public List<Product> getProductSearchResultsForId(Long id) {
		log.debug("ProductServiceImpl - getProductSearchResultsForId {}", id);
		
		String url = backendUrl + "/products/" + id.toString();
		log.debug("URL: {}", url);

		List<Product> productList = new ArrayList<>();
		
		ResponseEntity<Product[]> response = restTemplate.getForEntity(url, Product[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Product[] products = response.getBody();
			if(products.length > 0) {
				productList = new ArrayList<>(Arrays.asList(products));
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
			
		log.debug("ProductServiceImpl - productList has {} elements", productList.size());

		return productList;
	}


	@Override
	public Product getProductById(Long id) {
		log.debug("ProductServiceImpl - getProductById");
		log.debug("ProductServiceImpl - id = {}", id);

		String url = backendUrl + "/products/" + id.toString();
		log.debug("URL: {}", url);
		
		Product product = null;

		ResponseEntity<Product> response = restTemplate.getForEntity(url, Product.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			product = response.getBody();
			log.debug("product found: " + product.getProductNo());
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
			
		return product;
	}



	@Override
	@Transactional
	public Product saveProduct(Product product) {
		log.debug("ProductServiceImpl - saveProduct");
		log.debug("ProductServiceImpl - (before saving) id = {}", product.getId());
		
		Product savedProduct = null;
		
		if(product.getId() == null) {
			log.debug("new product");
			String url = backendUrl + "/products";
			log.debug("URL: {}", url);

			ResponseEntity<Product> response = restTemplate.postForEntity(url, product, Product.class);
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				savedProduct = response.getBody();
				log.debug("ProductServiceImpl - (after saving) id = {}", savedProduct.getId());
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		}
		else {
			log.debug("existing product");
			String url = backendUrl + "/products/" + product.getId().toString();
			log.debug("URL: {}", url);
			try {
				restTemplate.put(url, product);
				savedProduct = product;
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}

		return savedProduct;
	}

	@Override
	@Transactional
	public boolean deleteProductById(Long id) {
		log.debug("deleteProductById - deleteProduct");
		log.debug("deleteProductById Product: {}", id);
		
		String url = backendUrl + "/products/" + id.toString();
		log.debug("URL: {}", url);

		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			return false;
		}

		return true;
	}


	@Override
	public Product getProductByProductNo(String productNo) {
		
		String url = backendUrl + "/products"+ "?productNo=" + productNo;
		log.debug("URL: {}", url);
		
		Product product = null;
		
		ResponseEntity<Product[]> response = restTemplate.getForEntity(url, Product[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Product[] products = response.getBody();
			if(products.length > 0) {
				product = products[0];
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return product;
	}




}
