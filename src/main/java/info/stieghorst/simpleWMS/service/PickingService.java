package info.stieghorst.simpleWMS.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;

public interface PickingService {

	/*
	 * get a list of all picking orders
	 */
	List<PickingOrder> getCompletePickingOrderList();

	/* 
	 * get a single picking order, identified by ID
	 */
	PickingOrder getOrderById(Long id);

	/*
	 * get search results for picking order search,
	 * also complete list, if no search criteria
	 * using pagination
	 */
	Page<PickingOrder> getPickingOrderSearchResults(Date created, String status, String username, String assignment, Pageable pageable);

	/*
	 * save a single picking order
	 */
	PickingOrder savePickingOrder(PickingOrder order);

	/*
	 * save a single picking item
	 */
	PickingItem savePickingItem(PickingItem item);

	/*
	 * get a single picking by ID
	 */
	PickingItem getPickingItemById(Long id);

	/*
	 * delete a picking item
	 */
	void deletePickingItem(PickingItem item);

	/*
	 * get list of all picking items
	 */
	List<PickingItem> getCompletePickingItemList();

	/*
	 * delete a picking order
	 */
	void deletePickingOrder(PickingOrder order);


}
