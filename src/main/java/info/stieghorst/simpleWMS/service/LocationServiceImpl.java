package info.stieghorst.simpleWMS.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.Location;
import info.stieghorst.simpleWMS.domain.Stock;
import info.stieghorst.simpleWMS.transfer.LocationSearchTO;
import info.stieghorst.simpleWMS.transfer.RestPage;
import lombok.extern.slf4j.Slf4j;

/* 
 * service for all location operations
 */
@Slf4j
@Service
public class LocationServiceImpl implements LocationService {

	@Value("${backend.url}")
	private String backendUrl;

	private final RestTemplate restTemplate = new RestTemplate();

	private final ParameterizedTypeReference<RestPage<Location>> responseType 
		= new ParameterizedTypeReference<RestPage<Location>>() {};

	@Override
	public List<Location> getCompleteLocationList() {
		log.debug("LocationServiceImpl - getCompleteLocationList");

		String url = backendUrl + "/locations";
		log.debug("URL: {}", url);

		List<Location> locationList = new ArrayList<>();
		ResponseEntity<Location[]> response = restTemplate.getForEntity(url, Location[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Location[] locations = response.getBody();
			if (locations.length > 0) {
				locationList = new ArrayList<>(Arrays.asList(locations));
			} 
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		log.debug("LocationServiceImpl - locationList has {} elements", locationList.size());

		return locationList;
	}

	@Override
	public List<Location> getEmptyLocationList() {
		log.debug("LocationServiceImpl - getEmptyLocationList");

		String url = backendUrl + "/locations?locked=nein&noStock=true";
		log.debug("URL: {}", url);

		List<Location> locationList = new ArrayList<>();
		ResponseEntity<Location[]> response = restTemplate.getForEntity(url, Location[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Location[] locations = response.getBody();
			if (locations.length > 0) {
				locationList = new ArrayList<>(Arrays.asList(locations));
			} 
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		log.debug("LocationServiceImpl - locationList has {} elements", locationList.size());

		return locationList;
	}

	@Override
	public Page<Location> getLocationSearchResult(@Valid LocationSearchTO locationSearch, Pageable pageable) {
		log.debug("LocationServiceImpl - getLocationSearchResult");

		String url = backendUrl + "/paged/locations";

		Page<Location> page = null;

		String locked = locationSearch.getLocked();
		boolean noStock = locationSearch.isNoStock();
		Integer aisle = locationSearch.getAisle();
		Integer stack = locationSearch.getStack();
		Long id = locationSearch.getId();

		if (id != null) {
			url = url + "?id=" + id.toString() + "&pageNumber=" + pageable.getPageNumber() + "&pageSize="
					+ pageable.getPageSize();
		} else if (locked != null && locked.equals("ja")) {
			url = url + "?locked=ja" + "&pageNumber=" + pageable.getPageNumber() + "&pageSize="
					+ pageable.getPageSize();
		} else if (locked != null && locked.equals("nein")) {
			url = url + "?locked=nein" + "&pageNumber=" + pageable.getPageNumber() + "&pageSize="
					+ pageable.getPageSize();
		} else if (noStock) {
			String noStockString = noStock ? "true" : "false";
			url = url + "?noStock=" + noStockString + "&pageNumber=" + pageable.getPageNumber() + "&pageSize="
					+ pageable.getPageSize();
		} else if (aisle != null && aisle > 0 && stack != null && stack > 0) {
			url = url + "?aisle=" + aisle.toString() + "&stack=" + stack.toString() + "&pageNumber="
					+ pageable.getPageNumber() + "&pageSize=" + pageable.getPageSize();
		} else if (aisle != null && aisle > 0) {
			url = url + "?aisle=" + aisle.toString() + "&pageNumber=" + pageable.getPageNumber() + "&pageSize="
					+ pageable.getPageSize();
		} else {
			url = url + "?pageNumber=" + pageable.getPageNumber() + "&pageSize=" + pageable.getPageSize();
		}

		log.debug("URL: {}", url);
		ResponseEntity<RestPage<Location>> result = restTemplate.exchange(url, HttpMethod.GET, null/* httpEntity */,
				responseType);
		if(result.getStatusCode().equals(HttpStatus.OK)) {
			page = result.getBody();
		}
		else {
			log.error("REST request status code: {}", result.getStatusCode());
		}

		if(page != null) {
			for (Location location : page.getContent()) {
				log.debug("searching stock for location: {}/{}", location.getId(), location.getCoordinates());
				String stockUrl = backendUrl + "/stocks?locationId=" + location.getId().toString();
				log.debug("URL: {}", stockUrl);
				ResponseEntity<Stock[]> response = restTemplate.getForEntity(stockUrl, Stock[].class);
				if(response.getStatusCode().equals(HttpStatus.OK)) {
					Stock[] stockArray = response.getBody();
					if (stockArray.length > 0) {
						location.setStock(stockArray[0]);
					}
				}
				else {
					log.error("REST request status code: {}", response.getStatusCode());
				}
			}
		}

		return page;
	}

	@Override
	public List<Location> getLocationSearchResultsForId(Long id) {
		log.debug("LocationServiceImpl - getLocationSearchResultsForId {}", id);

		List<Location> locationList = new ArrayList<>();

		String url = backendUrl + "/locations/" + id.toString();
		log.debug("URL: {}", url);

		ResponseEntity<Location> response = restTemplate.getForEntity(url, Location.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Location location = response.getBody();
			locationList.add(location);	
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		log.debug("locationList - {} elements", locationList.size());
		return locationList;
	}

	@Override
	public Location getLocationById(Long id) {
		log.debug("LocationServiceImpl - getLocationById");
		log.debug("LocationServiceImpl - id = {}", id);

		String url = backendUrl + "/locations/" + id.toString();
		log.debug("URL: {}", url);

		Location location = null;
		
		ResponseEntity<Location> response = restTemplate.getForEntity(url, Location.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			location = response.getBody();
			log.debug("location found: " + location.getCoordinates());
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		return location;
	}

	@Override
	@Transactional
	public boolean deleteLocationById(Long id) {
		log.debug("LocationServiceImpl - deleteLocationById");
		log.debug("delete location, id = {}", id);

		String url = backendUrl + "/locations/" + id.toString();
		log.debug("URL: {}", url);
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			return false;
		}

		return true;
	}

	@Override
	public Location saveLocation(Location location) {
		log.debug("LocationServiceImpl - saveLocation");
		log.debug("LocationServiceImpl - (before saving) id = {}", location.getId());

		Location savedLocation = null;

		if (location.getId() == null) {
			log.debug("new location");
			String url = backendUrl + "/locations";
			log.debug("URL: {}", url);
			ResponseEntity<Location> response = restTemplate.postForEntity(url, location, Location.class);
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				savedLocation = response.getBody();
				log.debug("LocationServiceImpl - (after saving) id = {}", savedLocation.getId());
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		} else {
			log.debug("existing location");
			String url = backendUrl + "/locations/" + location.getId().toString();
			log.debug("URL: {}", url);
			try {
				restTemplate.put(url, location);
				savedLocation = location;
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}

		return savedLocation;
	}

	@Override
	public boolean createNewLocation(int aisle, int stack, int level) {
		log.debug("LocationServiceImpl - createNewLocation");
		log.debug("aisle: {}", aisle);
		log.debug("stack: {}", stack);
		log.debug("level: {}", level);

		int locationsCreated = insertLocation(aisle, stack, level);
		log.debug("{} new location were created", locationsCreated);

		return locationsCreated > 0;
	}

	@Override
	public boolean createNewAisle(int aisle, int noOfStacks, int noOfLevels) {
		log.debug("LocationServiceImpl - createNewAisle");
		log.debug("new aisle: {}", aisle);
		log.debug("number of stacks in new aisle: {}", noOfStacks);
		log.debug("number of levels in each stack: {}", noOfLevels);

		int locationsCreated = 0;
		for (int stack = 1; stack <= noOfStacks; stack++) {
			for (int level = 1; level <= noOfLevels; level++) {
				locationsCreated += insertLocation(aisle, stack, level);
			}
		}

		log.debug("{} new locations were created", locationsCreated);
		return locationsCreated > 0;
	}

	@Override
	public boolean createNewStack(int aisle, int stack, int noOfLevels) {
		log.debug("LocationServiceImpl - createNewStack");
		log.debug("new aisle/stack: {}/{}", aisle, stack);
		log.debug("number of levels in this stack: {}", noOfLevels);

		int locationsCreated = 0;
		for (int level = 1; level <= noOfLevels; level++) {
			locationsCreated += insertLocation(aisle, stack, level);
		}

		log.debug("{} new locations were created", locationsCreated);
		return locationsCreated > 0;
	}

	/*
	 * insert a new location (new = aisle/stack/level not already in table)
	 */
	private int insertLocation(int aisle, int stack, int level) {
		log.debug("LocationServiceImpl - insertLocation");

		Location location = new Location();
		location.setAisle(aisle);
		location.setStack(stack);
		location.setLevel(level);

		if (saveLocation(location) != null) {
			return 1;
		} else {
			return 0;
		}
	}

}
