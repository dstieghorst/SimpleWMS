package info.stieghorst.simpleWMS.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.stieghorst.simpleWMS.domain.Location;
import info.stieghorst.simpleWMS.transfer.LocationSearchTO;

public interface LocationService {

	/*
	 * get a list of all locations
	 */
	List<Location> getCompleteLocationList();

	/*
	 * get a list of all locations
	 * without stock
	 */
	List<Location> getEmptyLocationList();

	/* 
	 * get a location for a given ID
	 */
	Location getLocationById(Long id);

	/*
	 * delete a location for a given ID
	 */
	boolean deleteLocationById(Long id);

	/* 
	 * save the location 
	 */
	Location saveLocation(Location location);
	
	/*
	 * create a single location
	 */
	boolean createNewLocation(int aisle, int stack, int level);
	
	/*
	 * create all locations for a new aisle
	 * don't change existing locations in this aisle
	 */
	boolean createNewAisle(int aisle, int noOfStacks, int noOfLevels);
	
	/*
	 * create all locations for a new stack
	 * don't change existing locations in this stack
	 */
	boolean createNewStack(int aisle, int stack, int noOfLevels);

	/*
	 * find "all" locations with the given id
	 * only 1 result, but in a list
	 */
	public List<Location> getLocationSearchResultsForId(Long id);
	
	/*
	 * get all locations matching the given search criteria
	 */
	public Page<Location> getLocationSearchResult(@Valid LocationSearchTO locationSearch, Pageable pageable);

}
