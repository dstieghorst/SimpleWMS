package info.stieghorst.simpleWMS.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import info.stieghorst.simpleWMS.domain.Stock;
import info.stieghorst.simpleWMS.transfer.RestPage;
import lombok.extern.slf4j.Slf4j;

/* 
 * service for all stock operations
 */
@Slf4j
@Service
public class StockServiceImpl implements StockService {
	
	@Value("${backend.url}")
    private String backendUrl;

    private final RestTemplate restTemplate = new RestTemplate();
    
    private final ParameterizedTypeReference<RestPage<Stock>> responseType 
    	= new ParameterizedTypeReference<RestPage<Stock>>() { };

	@Override
	public List<Stock> getCompleteStockList() {
		log.debug("StockServiceImpl - getCompleteStockList");
		
		String url = backendUrl + "/stocks";
		log.debug("URL: {}", url);
		
		List<Stock> stockList = new ArrayList<>();
		
		ResponseEntity<Stock[]> response = restTemplate.getForEntity(url, Stock[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Stock[] stocks = response.getBody();
			if(stocks.length > 0) {
				stockList = new ArrayList<>(Arrays.asList(stocks));
			}
			else {
				log.warn("no stocks found!");
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		
		return stockList;
	}

	@Override
	public Stock getStockById(Long id) {
		log.debug("StockServiceImpl - getStockById");
		
		String url = backendUrl + "/stocks/" + id.toString();
		log.debug("URL: {}", url);
		
		Stock stock = null;
		
		ResponseEntity<Stock> response = restTemplate.getForEntity(url, Stock.class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			stock = response.getBody();
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}

		return stock;
	}

	@Override
	public Page<Stock> getStockSearchResult(String productNo, boolean noLocation, Integer aisle, Integer stack, Integer level, Pageable pageable) {
		log.debug("StockServiceImpl - getStockSearchResult");
		
		String url = backendUrl + "/paged/stocks";

		Page<Stock> page = null;
		
		if(productNo != null && productNo.trim().length() > 0) {
			String productNoWildcards = "%" + productNo + "%";
			url = url + "?productNo=" + productNoWildcards
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else if(noLocation) {
			url = url + "?noLocation=true"
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else if(aisle != null && aisle > 0 
				&& (stack == null || stack == 0)) {
			url = url + "?aisle=" + aisle.toString()
					+ "&pageNumber=" + pageable.getPageNumber() 
					+ "&pageSize=" + pageable.getPageSize();
		}
		else if(aisle != null && aisle > 0 
				&& stack != null && stack > 0
				&&( level == null || level == 0)) {
			url = url + "?aisle=" + aisle.toString()
				+ "&stack=" + stack.toString()
				+ "&pageNumber=" + pageable.getPageNumber() 
				+ "&pageSize=" + pageable.getPageSize();
		}
		else if(aisle != null && aisle > 0 
				&& stack != null && stack > 0
				&& level != null && level > 0) {
			url = url + "?aisle=" + aisle.toString()
			+ "&stack=" + stack.toString()
			+ "&level=" + level.toString()
			+ "&pageNumber=" + pageable.getPageNumber() 
			+ "&pageSize=" + pageable.getPageSize();
		}
		else {
			url = url + "?pageNumber=" + pageable.getPageNumber() 
			+ "&pageSize=" + pageable.getPageSize();
		}

		log.debug("URL: {}", url);
		
		ResponseEntity<RestPage<Stock>> result = restTemplate.exchange(url, HttpMethod.GET, null/*httpEntity*/, responseType);
		if(result.getStatusCode().equals(HttpStatus.OK)) {
			page = result.getBody();
			log.debug("StockServiceImpl - stockList has {} elements", page.getContent().size());
		}
		else {
			log.error("REST request status code: {}", result.getStatusCode());
		}
		
		return page;
	}

	@Override
	public List<Stock> getStockSearchResultsForProduct(Long id) {
		log.debug("StockServiceImpl - getStockSearchResultsForProduct - id = {}", id);
		
		String url = backendUrl + "/stocks?productId=" + id.toString();
		log.debug("URL: {}", url);
		
		List<Stock> stockList = new ArrayList<>();
		
		ResponseEntity<Stock[]> response = restTemplate.getForEntity(url, Stock[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Stock[] stocks = response.getBody();
			if(stocks.length > 0) {
				stockList = new ArrayList<>(Arrays.asList(stocks));
			}
			else {
				log.warn("no stocks found!");
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return stockList;
	}

	@Override
	public List<Stock> getStockSearchResultsForLocation(Long id) {
		log.debug("StockServiceImpl - getStockSearchResultsForLocation - id = {}", id);
		
		String url = backendUrl + "/stocks?locationId=" + id.toString();
		log.debug("URL: {}", url);
		
		List<Stock> stockList = new ArrayList<>();
		
		ResponseEntity<Stock[]> response = restTemplate.getForEntity(url, Stock[].class);
		if(response.getStatusCode().equals(HttpStatus.OK)) {
			Stock[] stocks = response.getBody();
			if(stocks.length > 0) {
				stockList = new ArrayList<>(Arrays.asList(stocks));
			}
			else {
				log.warn("no stocks found!");
			}
		}
		else {
			log.error("REST request status code: {}", response.getStatusCode());
		}
		
		return stockList;
	}

	@Override
	@Transactional
	public Stock saveStock(Stock stock) {
		log.debug("StockServiceImpl - saveStock");
		
		Stock savedStock = null;
		
		if(stock.getId() == null) {
			log.debug("new stock");
			String url = backendUrl + "/stocks";
			log.debug("URL: {}", url);
			ResponseEntity<Stock> response = restTemplate.postForEntity(url, stock, Stock.class);
			if(response.getStatusCode().equals(HttpStatus.OK)) {
				savedStock = response.getBody();
			}
			else {
				log.error("REST request status code: {}", response.getStatusCode());
			}
		}
		else {
			log.debug("existing stock");
			String url = backendUrl + "/stocks/" + stock.getId().toString();
			log.debug("URL: {}", url);
			try {
				restTemplate.put(url, stock);
				savedStock = stock;
			} catch(HttpClientErrorException httpClientExc) {
				log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			}
		}
		
		return savedStock;
	}

	@Override
	@Transactional
	public boolean deleteStock(Stock stock) {
		log.debug("StockServiceImpl - deleteStock {}", stock.getId());
		
		String url = backendUrl + "/stocks/" + stock.getId().toString();
		log.debug("URL: {}", url);
		try {
			restTemplate.delete(url);
		} catch(HttpClientErrorException httpClientExc) {
			log.error("Rest request exception - status code: {}", httpClientExc.getStatusCode());
			return false;
		}

		return true;
	}

}
