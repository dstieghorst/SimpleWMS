package info.stieghorst.simpleWMS.service;

import java.util.List;

import info.stieghorst.simpleWMS.domain.Role;
import info.stieghorst.simpleWMS.domain.User;

public interface UserService {
	
	/*
	 * get a list of all users
	 */
	List<User> getCompleteUserList();
	
	/*
	 * get a list of all user having a certain role
	 */
	List<User> getUsersIncludingRole(Role role);

	/* 
	 * find user by ID
	 */
	User getUserById(Long id);

	/*
	 * find user by user name
	 */
	User getUserByName(String userName);
	
	/*
	 * save a newly created user
	 */
	User saveNewUser(User user);

	/* 
	 * save an existing user
	 */
	void saveUser(User user);

	/*
	 * save an existing user
	 * and change the password
	 */
	void saveUser(User user, String password);

	/*
	 * delete a user, identified by ID
	 */
	void deleteUserById(Long id);

	/*
	 * get a list of all roles
	 */
	List<Role> getCompleteRoleList();
	
	/*
	 * get a single role by ID
	 */
	Role getRoleById(Long id);

	/* 
	 * save a newly created role
	 */
	Role saveNewRole(Role role);
	
	/*
	 * save an existing role
	 */
	void saveRole(Role role);

	/*
	 * delete an existing role,
	 * identified by ID
	 */
	void deleteRoleById(Long id);

}
