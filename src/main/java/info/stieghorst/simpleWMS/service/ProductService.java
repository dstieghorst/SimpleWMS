package info.stieghorst.simpleWMS.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import info.stieghorst.simpleWMS.domain.Product;

/*
 * service interface for the product class
 */
public interface ProductService {
	
	/*
	 * get all products
	 */
	public List<Product> getCompleteProductList();

	/*
	 * get all products that are active and have stock
	 */
	public List<Product> getAvailableProducts();

	/*
	 * find all products matching the given search criteria
	 */
	public Page<Product> getProductSearchResult(String productNo, String name, String description, Pageable pageable);

	/*
	 * find "all" products with the given id
	 * only 1 result, but in a list
	 */
	public List<Product> getProductSearchResultsForId(Long id);

	/*
	 * get a single product
	 */
	public Product getProductById(Long id);

	/* 
	 * save a product (update or insert)
	 */
	public Product saveProduct(Product product);
	
	/*
	 * delete a product with a given ID
	 */
	public boolean deleteProductById(Long id);

	/*
	 * find product by productNo
	 */
	public Product getProductByProductNo(String productNo);

}
