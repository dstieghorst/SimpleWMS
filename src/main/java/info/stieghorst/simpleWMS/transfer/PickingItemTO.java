package info.stieghorst.simpleWMS.transfer;

import info.stieghorst.simpleWMS.domain.PickingItem;
import lombok.Data;

/*
 * DTO for locations 
 * fields of the original class 
 * reduced related objects to the name attribute
 * and web template
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */

@Data
public class PickingItemTO {

	private Long id;
	
	private Long orderId;
	
	private String orderName;
	
	private String productNo;
	
	private Long stockId;
	
	private String stockLocation;
	
	private Integer orderQuantity;
	
	private Integer actualQuantity;
	
	public PickingItemTO() {
		
	}
	
	public PickingItemTO(PickingItem item) {
		this.id = item.getId();
		this.orderId = item.getOrder().getId();
		this.orderName = item.getOrder().getOrderName();
		this.productNo = item.getProduct().getProductNo();
		this.orderQuantity = item.getOrderQuantity();
		this.actualQuantity = item.getActualQuantity();
	}
	
}


