package info.stieghorst.simpleWMS.transfer;

import info.stieghorst.simpleWMS.domain.User;
import lombok.Data;

/*
 * DTO for user password change 
 * some fields of the original class 
 * plus some more for use in controller
 * and web template
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */

@Data
public class UserPasswordTO {

	private Long id;
	
	private String username;
	
	private String oldPassword;
	
	private String oldPasswordInput;
	
	private String newPassword1;
	
	private String newPassword2;
	
	public UserPasswordTO() {
		
	}
	
	public UserPasswordTO(User user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.oldPassword = user.getPassword();
		
		this.oldPasswordInput = "";
		this.newPassword1 = "";
		this.newPassword2 = "";
	}
	
}
