package info.stieghorst.simpleWMS.transfer;

import info.stieghorst.simpleWMS.domain.Product;
import lombok.Data;

/*
 * DTO for creating new stock 
 * fields of the Product class
 * plus units (= count) field 
 * plus some more for use in controller
 * and web template
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class NewStockTO {

	private Long id;

	private String productNo;
	
	private String name;
	
	private String description;
	
	private boolean active;
	
	private Integer units;
	
	public NewStockTO(Product product) {
		if(product != null) {
			this.id = product.getId();
			this.productNo = product.getProductNo();
			this.name = product.getName();
			this.description = product.getDescription();
			this.active = product.isActive();
			this.units = 0;
		}
	}

	public Product getProduct() {
		Product product = new Product();
		
		product.setId(id);
		product.setProductNo(productNo);
		product.setName(name);
		product.setDescription(description);
		product.setActive(active);
		
		return product;
	}
	
}
