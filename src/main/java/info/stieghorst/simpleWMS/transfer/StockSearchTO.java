package info.stieghorst.simpleWMS.transfer;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import lombok.Data;

/*
 * DTO for the stock search 
 * include fields for search criteria input:
 * product.productNo, location.aisle, location.stack, location.level
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class StockSearchTO {
	
	/*
	 * partial product number
	 * will be used in a LIKE expression
	 * 
	 * if a partial product no is entered,
	 * it has to be at least 3 characters
	 * 
	 * if none is entered, the controller
	 * will treat it is a null value
	 */
	@Size(min=2, message="Es müssen mindestens 2 Zeichen der Artikelnummer eingegeben werden")
	private String productNo;
	
	/*
	 * aisle number - if entered, 
	 * it has to be between 1 and 999
	 */
	@Min(value = 1, message="Die Gangnummer muss mindestens 1 sein")
	@Max(value = 999, message="Die Gangnummer darf höchstens 999 sein")
	private Integer locAisle;
	
	/*
	 * stack number - if entered, 
	 * it has to be between 1 and 999
	 */
	@Min(value = 1, message="Die Säulennummer muss mindestens 1 sein")
	@Max(value = 999, message="Die Säulennummer darf höchstens 999 sein")
	private Integer locStack;
	
	/*
	 * level number - if entered, 
	 * it has to be between 1 and 999
	 */
	@Min(value = 1, message="Die Ebenennummer muss mindestens 1 sein")
	@Max(value = 999, message="Die Ebenennummer darf höchstens 999 sein")
	private Integer locLevel;
	
	private boolean noLocation;

}
