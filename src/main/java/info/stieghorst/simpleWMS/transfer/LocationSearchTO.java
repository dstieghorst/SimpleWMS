package info.stieghorst.simpleWMS.transfer;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import lombok.Data;

/*
 * DTO for the location search 
 * include fields for search criteria input
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class LocationSearchTO {
	
	private Long id;
	
	private String locked;
	
	private boolean noStock;
	
	@Min(value = 1, message="Die Gangnummer muss mindestens 1 sein")
	@Max(value = 999, message="Die Gangnummer darf höchstens 999 sein")
	private Integer aisle;
	
	@Min(value = 1, message="Die Säulennummer muss mindestens 1 sein")
	@Max(value = 999, message="Die Säulennummer darf höchstens 999 sein")
	private Integer stack;
	
	public LocationSearchTO() {
		id = null;
		locked = "egal";
		aisle = null;
		stack = null;
		noStock = false;
	}
}
