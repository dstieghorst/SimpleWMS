package info.stieghorst.simpleWMS.transfer;

import javax.validation.constraints.Min;

import info.stieghorst.simpleWMS.domain.Stock;
import lombok.Data;

/*
 * DTO for the stock search 
 * include fields for search criteria input:
 * product.productNo, location.aisle, location.stack, location.level
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class StockCorrectionTO {
	
	Long id;
	
	String productNo;
	
	String locCoordinates;
	
	@Min(0)
	Integer units;
	
	public StockCorrectionTO() {
		
	}
	
	public StockCorrectionTO(Stock stock) {
		this.id = stock.getId();
		this.productNo = stock.getProduct().getProductNo();
		this.locCoordinates = stock.getLoc().getCoordinates();
		this.units = stock.getUnits();
	}

}
