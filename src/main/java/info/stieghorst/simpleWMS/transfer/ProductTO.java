package info.stieghorst.simpleWMS.transfer;

import info.stieghorst.simpleWMS.domain.Product;
import lombok.Data;

/*
 * DTO for products 
 * fields of the original class 
 * plus some more for use in controller
 * and web template
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */

@Data
public class ProductTO {
	
	private Long id;

	private String productNo;
	
	private String name;
	
	private String description;
	
	private boolean active;
	
	private boolean editDisabled;
	
	private String transferMode;
	
	public ProductTO(Product product) {
		if(product != null) {
			this.id = product.getId();
			this.productNo = product.getProductNo();
			this.name = product.getName();
			this.description = product.getDescription();
			this.active = product.isActive();
		}
	}
	
	public Product getProduct() {
		Product product = new Product();
		product.setId(this.id);
		product.setProductNo(this.productNo);
		product.setName(this.name);
		product.setDescription(this.description);
		product.setActive(this.active);
		
		return product;
	}
}
