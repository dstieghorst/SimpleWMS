package info.stieghorst.simpleWMS.transfer;

import info.stieghorst.simpleWMS.domain.Location;
import lombok.Data;

/*
 * DTO for locations 
 * fields of the original class 
 * plus some more for use in controller
 * and web template
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */

@Data
public class LocationTO {

	private Long id;

	private Integer aisle;
	
	private Integer stack;
	
	private Integer level;
	
	private Boolean locked;

	private boolean editDisabled;
	
	private String transferMode;
	
	public LocationTO(Location location) {
		if(location != null) {
			this.id = location.getId();
			this.aisle = location.getAisle();
			this.stack = location.getStack();
			this.level = location.getLevel();
			this.locked = location.getLocked();
		}
	}
	
	public Location getLocation() {
		Location location = new Location();
		location.setId(id);
		location.setAisle(aisle);
		location.setStack(stack);
		location.setLevel(level);
		location.setLocked(locked);
		
		return location;
	}

	public String getCoordinates() {
		return String.format("%03d", aisle)
				+ "-"
				+ String.format("%03d", stack)
				+ "-"
				+ String.format("%03d", level);
	}

	
}
