package info.stieghorst.simpleWMS.transfer;

import javax.validation.constraints.Size;

import lombok.Data;

/*
 * DTO for the product search 
 * include fields for search criteria input
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class ProductSearchTO {

	/*
	 * partial product number
	 * will be used in a LIKE expression
	 * 
	 * if a partial product no is entered,
	 * it has to be at least 3 characters
	 * 
	 * if none is entered, the controller
	 * will treat it is a null value
	 */
	@Size(min=3, message="Artikelnummer: Bitte mindestens 3 Zeichen eingeben!")
	private String productNo;
	
	/*
	 * partial product name
	 * will be used in a LIKE expression
	 * 
	 * if a partial product no is entered,
	 * it has to be at least 3 characters
	 * 
	 * if none is entered, the controller
	 * will treat it is a null value
	 */
	@Size(min=3, message="Artikelbezeichnung: Bitte mindestens 3 Zeichen eingeben!")
	private String name;
	
	/*
	 * partial product description
	 * will be used in a LIKE expression
	 * 
	 * if a partial product no is entered,
	 * it has to be at least 3 characters
	 * 
	 * if none is entered, the controller
	 * will treat it is a null value
	 */
	@Size(min=3, message="Beschreibung: Bitte mindestens 3 Zeichen eingeben!")
	private String description;
	

}
