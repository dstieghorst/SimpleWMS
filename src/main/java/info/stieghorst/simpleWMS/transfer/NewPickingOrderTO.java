package info.stieghorst.simpleWMS.transfer;

import lombok.Data;

/*
 * DTO for creating a new picking order 
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class NewPickingOrderTO {
	
	private String orderName;

}
