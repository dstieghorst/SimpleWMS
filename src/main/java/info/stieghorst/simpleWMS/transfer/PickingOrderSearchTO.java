package info.stieghorst.simpleWMS.transfer;



import lombok.Data;

/*
 * DTO for the picking order search 
 * include fields for search criteria input
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class PickingOrderSearchTO {
	
	private String created;
	
	private String orderStatus;
	
	private String assignment;

}
