package info.stieghorst.simpleWMS.transfer;

import info.stieghorst.simpleWMS.domain.PickingItem;
import info.stieghorst.simpleWMS.domain.PickingOrder;
import info.stieghorst.simpleWMS.domain.Product;
import lombok.Data;

/*
 * DTO for creating a new picking item 
 *  
 * using Projekt Lombok (@Data) to have 
 * getters and setters etc.
 * created automatically
 */
@Data
public class NewPickingItemTO {
	
	private Long orderId;
	
	private String productNo;
	
	private Integer orderQuantity;

	public NewPickingItemTO(Long orderId) {
		this.orderId = orderId;
	}
	
	public PickingItem getPickingItem(PickingOrder order, Product product) {
		PickingItem newItem = new PickingItem();
		newItem.setOrder(order);
		newItem.setProduct(product);
		newItem.setItemStatus(PickingItem.STATUS_OPEN);
		newItem.setOrderQuantity(orderQuantity);
		newItem.setActualQuantity(0);
		
		return newItem;
	}

}
